package com.gstudio.danil.myapplication.ui.profile.fragment

import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.ProfileDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ProfileFragmentPresenterImp @Inject constructor(
  private val profileFragmentView: ProfileFragmentView,
  private val apiService: Endpoints,
  private val router: Router
) : BasePresenter<ProfileFragmentView>(router), ProfileFragmentPresenter {

  fun getProfileData() {
    disposable.add(
        apiService.getProfile()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: ProfileDTO ->
              profileFragmentView.putDataToViews(
                  userName = response.name,
                  imageUrl = response.imageUrl,
                  restaurantName = response.restaurant?.name,
                  function = response.position,
                  checklistsDone = response.checkListsDone,
                  checklistsTotal = response.checkListsTotal
              )
            },
                { e: Throwable ->
                  val errorBody = (e as? HttpException)?.response()
                      ?.errorBody()
                  println(errorBody)
                })
    )
  }
}