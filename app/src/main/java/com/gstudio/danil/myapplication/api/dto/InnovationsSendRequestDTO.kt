package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class InnovationsShareDTO(
  @SerializedName("innovation") val innovation: Innovation,
  @SerializedName("text") val text: String
)

data class InnovationsShareRequest(
  @SerializedName("category_id") val categoryId: Int,
  @SerializedName("idea") val ideaText: String
)

data class Innovation(
  @SerializedName("id") val id: Int,
  @SerializedName("user_id") val user_id: Int,
  @SerializedName("category_id") val categoryId: Int,
  @SerializedName("idea") val idea: String
//  @SerializedName("images") val images: Image
// не парсим поле images, в котором лежит объект

)