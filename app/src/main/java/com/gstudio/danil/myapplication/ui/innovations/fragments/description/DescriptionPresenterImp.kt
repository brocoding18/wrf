package com.gstudio.danil.myapplication.ui.innovations.fragments.description

import android.content.Context
import android.util.Log
import com.gstudio.danil.myapplication.Screens
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.InnovationsPhotoPostDTO
import com.gstudio.danil.myapplication.api.dto.InnovationsShareDTO
import com.gstudio.danil.myapplication.api.dto.InnovationsShareRequest
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import com.gstudio.danil.myapplication.utils.PictureUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import java.io.File
import javax.inject.Inject

class DescriptionPresenterImp @Inject constructor(
  val context: Context,
  val descriptionView: DescriptionView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<DescriptionView>(router), DescriptionPresenter {

  private var innovationPostText: String? = null
  private var innovationId: Int? = null

  fun onShareButtonClick(innovationObject: InnovationsShareRequest) {
    MessageFragment.isLastPage = true

    val request = createRequest(innovationObject)
    disposable.add(request)
  }

  private fun createRequest(innovationObject: InnovationsShareRequest): Disposable {
    if (DescriptionFragment.canTakePhoto) {
      return apiService.postInnovation(innovationObject)
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: InnovationsShareDTO ->

            innovationId = response.innovation.id
            innovationPostText = response.text
            goLastScreen()
          },
              { e: Throwable ->
                val errorBody = (e as HttpException).response()
                    .errorBody()
                println(errorBody)
              }
          )
    } else {
      return apiService.postInnovation(innovationObject)
          .flatMap { response: InnovationsShareDTO ->
            innovationId = response.innovation.id
            innovationPostText = response.text

            val imagePath = File(spHelper.getPhotoPath()).absolutePath
            val file = PictureUtils.getCompressed(context, imagePath!!)

            val requestFile = RequestBody.create(MediaType.parse("image/jpg"), file)
            val body = MultipartBody.Part.createFormData("file", file.name, requestFile)

            return@flatMap apiService.postInnovationPhoto(innovationId!!, body)
          }
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: InnovationsPhotoPostDTO ->

            println(response)

            deleteImagePath()

            goLastScreen()
          },
              { e: Throwable ->
                // TODO убери этот костыль, когда починят Бэк
                val errorBody = (e as HttpException).response()
                    .errorBody()

                Log.e("InnovationsDescription", errorBody?.string())
                if (e.code() == 400) {
                  deleteImagePath()

                  goLastScreen()
                }
              })
    }
  }

  fun goLastScreen() {
    router.newRootChain(Screens.InnovationsMessageScreen(innovationPostText))
  }

  fun getPhotoPath(): String? = spHelper.getPhotoPath()

  fun saveImagePath(photoPath: String) {
    spHelper.setPhotoPath(photoPath)
  }

  fun deleteImagePath() {
    spHelper.setPhotoPath(null)
  }

}