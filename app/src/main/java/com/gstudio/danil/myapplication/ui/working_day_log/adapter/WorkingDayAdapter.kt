package com.gstudio.danil.myapplication.ui.working_day_log.adapter

import android.arch.paging.PagedListAdapter
import android.view.ViewGroup
import com.gstudio.danil.myapplication.R.layout
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder.WorkingDayHolder
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder.WorkingDayNotebookHolder
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder.WorkingDayStoryHolder
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder.WorkingDayTemperatureHolder
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayStoryItem
import com.gstudio.danil.myapplication.ui.working_day_log.diff_util.WorkingDayDiffUtil.diffCallback
import com.gstudio.danil.myapplication.utils.ext.inflate

class WorkingDayAdapter(
  private val clickListenerStory: OnStoryItemClickListener? = null
) : PagedListAdapter<WorkingDayItem, WorkingDayHolder>(diffCallback) {

  override fun onBindViewHolder(parent: WorkingDayHolder, position: Int) {
    parent.bindItem(getItem(position)!!)
  }
  override fun getItemViewType(position: Int): Int = getItem(position)!!.getItemType()

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WorkingDayHolder =
    when (viewType) {
      0 -> WorkingDayStoryHolder(parent.inflate(layout.working_day_story_item), clickListenerStory)
      1 -> WorkingDayTemperatureHolder(parent.inflate(layout.working_day_temperature_item))
      2 -> WorkingDayNotebookHolder(parent.inflate(layout.working_day_notebook_item))
      else -> throw Exception("Nonexisted viewType")
    }

  interface OnStoryItemClickListener {
    fun onItemClick(storyItem: WorkingDayStoryItem)
  }
}
