package com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list

import android.arch.paging.DataSource
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import javax.inject.Inject

class SearchDataSourceFactory @Inject constructor(private val searchDataSource: SearchDataSource) :
    DataSource.Factory<Int, WorkingDayItem>() {

  override fun create(): DataSource<Int, WorkingDayItem> = searchDataSource

}