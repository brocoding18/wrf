package com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.ext.inflate
import javax.inject.Inject

class MessageFragment : BaseFragment(), MessageView {
  companion object {
    var isLastPage = false
    private const val ARG_INNOVATIONS_MESSAGE = "innovations_message"

    fun newInstance(message: String?): MessageFragment {
      val args = Bundle()
      args.putString(ARG_INNOVATIONS_MESSAGE, message)

      val fragment = MessageFragment()
      fragment.arguments = args
      return fragment
    }
  }

  private lateinit var innovationsMessage: String

  @BindView(R.id.innovations_hello_middle_message) lateinit var middleMessage: TextView
  @BindView(R.id.innovations_hello_bottom_message) lateinit var bottomMessage: TextView

  @Inject lateinit var presenter: MessagePresenterImp

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    innovationsMessage = arguments!!.getString(ARG_INNOVATIONS_MESSAGE)!!
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.innovations_hello_bye_fragment)
    ButterKnife.bind(this, view)

    middleMessage.text = innovationsMessage
    bottomMessage.text = "Спасибо"

    return view
  }

  override fun onResume() {
    super.onResume()
  }

  @OnClick(R.id.innovations_hello_container)
  override fun onLayoutClick() {
    if (isLastPage) {
      activity!!.onBackPressed()
    }

    presenter.onLayoutClick()
  }
}
