package com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook

import android.arch.lifecycle.Observer
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.arch.paging.PagedList.Config
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.support.v7.widget.Toolbar
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.Constants.WorkingDay.SEARCH_STRING
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.WorkingDayAdapter
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.WorkingDayAdapter.OnStoryItemClickListener
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.WorkingDaySearchAdapter
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayLab
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayStoryItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.notebook.WorkingDayDataSourceFactory
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list.SearchDataSource
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list.SearchDataSourceFactory
import com.gstudio.danil.myapplication.utils.RecyclerItemDecorator
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import java.util.concurrent.Executors
import javax.inject.Inject

class NotebookFragment : BaseFragment(), NotebookView {
  companion object {
    val TAG = NotebookFragment::class.java.simpleName
    fun newInstance(): NotebookFragment = NotebookFragment()
  }

  private lateinit var adapterFull: WorkingDayAdapter
  private lateinit var adapterSearch: WorkingDaySearchAdapter
  private lateinit var adapterDetail: WorkingDaySearchAdapter

  @BindView(R.id.util_general_header) lateinit var headerText: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar) lateinit var toolbar: Toolbar
  @BindView(R.id.working_day_notebook_recycler) lateinit var recyclerView: RecyclerView
  @BindView(R.id.util_toolbar_right_side_text) lateinit var toolbarRightSideText: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout

  @BindView(R.id.working_day_notebook_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: NotebookPresenterImp
  @Inject lateinit var dataSourceFactory: WorkingDayDataSourceFactory
  @Inject lateinit var pagedAdapterConfig: Config

  @Inject lateinit var searchDataSourceFactory: SearchDataSourceFactory
  @Inject lateinit var searchDataSource: SearchDataSource

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = inflater.inflate(R.layout.working_day_notebook_fragment, container, false)
    ButterKnife.bind(this, view)

    setupToolbar()

    return view
  }

  override fun onResume() {
    super.onResume()

    presenter.getEvents()
  }

  override fun onCreateOptionsMenu(
    menu: Menu?,
    inflater: MenuInflater?
  ) {
    inflater!!.inflate(R.menu.working_day_search_item, menu)

    val searchMenuItem = menu!!.findItem(R.id.working_day_search)
    val searchView = searchMenuItem.actionView as SearchView

    searchView.apply {
      maxWidth = Int.MAX_VALUE
      imeOptions = EditorInfo.IME_ACTION_SEARCH
    }

    setSearchListeners(searchView, searchMenuItem)

    super.onCreateOptionsMenu(menu, inflater)
  }

  private fun setupToolbar() {
    (activity!! as AppCompatActivity).setSupportActionBar(toolbar)

    headerText.text = "Журнал Смены"
    hideAndShowViews(viewsToGone = listOf(headerDetail))
    headerBackButton.setOnClickListener { onUpButtonClick() }

    toolbarRightSideText.text = "Завершить смену"
  }

  override fun setupNotebookAdapter() {

    adapterFull = WorkingDayAdapter(object : OnStoryItemClickListener {
      override fun onItemClick(storyItem: WorkingDayStoryItem) {
        presenter.goToDetail(storyItem.id, storyItem.person, storyItem.date)
      }
    })

    val pagedLiveList = LivePagedListBuilder(dataSourceFactory, pagedAdapterConfig)
        .setFetchExecutor(Executors.newSingleThreadExecutor())
        .setBoundaryCallback(object : PagedList.BoundaryCallback<WorkingDayItem>() {
          override fun onItemAtFrontLoaded(itemAtEnd: WorkingDayItem) {
            scrollToTempPos()
            hideAndShowViews(viewsToShow = listOf(recyclerView), viewsToHide = listOf(progressBar))
          }
        })
        .build()

    pagedLiveList.observe(this, Observer<PagedList<WorkingDayItem>> {
      adapterFull.submitList(it)
    })

    recyclerView.apply {
      adapter = adapterFull
      layoutManager = LinearLayoutManager(activity!!)
      addItemDecoration(RecyclerItemDecorator(resources.getDimension(R.dimen.working_day_notebook_bottom_padding)))
    }
  }

  // TODO не скроллит =(
  override fun scrollToTempPos() {
    recyclerView.scrollToPosition(WorkingDayLab.getTemperaturePos() + 2)
  }

  private fun setSearchListeners(
    searchView: SearchView,
    searchMenuItem: MenuItem
  ) {
    searchMenuItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {
      override fun onMenuItemActionExpand(item: MenuItem?): Boolean {
        headerText.text = "Поиск в журнале"
        setupSearchAdapter()

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
          override fun onQueryTextSubmit(text: String?): Boolean {
            SEARCH_STRING = text

            // TODO инвалидэйт не рабоатет ((
//            searchDataSource.invalidate()
            setupSearchAdapter()

            return false
          }

          override fun onQueryTextChange(text: String?): Boolean {
            if (text == "" || text == null) {
              SEARCH_STRING = text
              setupSearchAdapter()
            }
            return false
          }
        })

        return true
      }

      override fun onMenuItemActionCollapse(item: MenuItem?): Boolean {
        headerText.text = "Журнал смены"

        SEARCH_STRING = ""

        recyclerView.apply {
          adapter = adapterFull
//          scrollToPosition(WorkingDayLab.getTemperaturePos() - 2)
        }
        return true
      }
    })
  }

  private fun setupSearchAdapter() {
    adapterSearch = WorkingDaySearchAdapter()

    val pagedLiveList = LivePagedListBuilder(searchDataSourceFactory, pagedAdapterConfig)
        .setFetchExecutor(Executors.newSingleThreadExecutor())
        .setBoundaryCallback(object : PagedList.BoundaryCallback<WorkingDayItem>() {
          override fun onItemAtFrontLoaded(itemAtEnd: WorkingDayItem) {
//            scrollToTempPos()
//            hideAndShowViews(viewsToShow = listOf(recyclerView), viewsToHide = listOf(progressBar))
          }
        })
        .build()

    pagedLiveList.observe(this, Observer<PagedList<WorkingDayItem>> {
      adapterSearch.submitList(it)
    })

    recyclerView.apply {
      adapter = adapterSearch
      layoutManager = LinearLayoutManager(activity!!)
      addItemDecoration(RecyclerItemDecorator(resources.getDimension(R.dimen.working_day_notebook_bottom_padding)))
    }
  }

  override fun notifySearchResults() {
    // TODO возможно, не пригодится
//    adapterSearch.notifyAdapter()
  }



  @OnClick(R.id.util_toolbar_right_side_text)
  fun onCloseWorkingDayClick() {
    AlertDialog.Builder(activity!!)
        .setTitle("Вы уверены?")
        .setPositiveButton("Да") { _, _ ->  presenter.onWorkingDayFinish() }
        .setNegativeButton("Нет") {dialog, _ -> dialog.dismiss() }
        .create()
        .show()
  }

  @OnClick(R.id.working_day_notebook_add_event)
  fun onAddEventClick() {
    presenter.addEventScreen()
  }
}