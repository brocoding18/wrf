package com.gstudio.danil.myapplication.entities

import java.util.*

data class ChecklistModel(
  val id: Int,
  val category: String,
  val title: String,
  val time: Calendar,
  val description: String,
    // 0 - черный, 1 - малиновый, 2 - зеленый, 3 - желтый
  var status: Int = 0
)

// TODO убрать генератор, когда появится БЭК

object ChecklistLab {
  private var mChecklistModels: MutableList<ChecklistModel> = ArrayList()

  init {
    for (i in 1..50) {
      var category = "Свет"
      var title = "Проверка света"
      var description =
        "Пожалуйста проверьте весь свет в помещение на предмет замыкания и перегорания световых приборов."
      //TODO убрать темп, когда будут внешние данные
      val time = Calendar.getInstance()
      val temp = time.clone() as Calendar
      if (i % 10 == 0) {
        temp.add(Calendar.MINUTE, -48)
      } else {
        temp.add(Calendar.MINUTE, 48)
      }
      time.time = temp.time
//      val sdf = SimpleDateFormat("hh:mm aa", Locale.US)
//      val formattedTime = sdf.format(time.time)
      if (i % 2 == 0) {
        category = "Вход"
        title = "Проверка входной группы"
        description =
            "Пожалуйста проверте аккуратность входной группы, вывесок, рекламы, урн. помните все что вы видите является частью нашего ресторана."
      } else if (i % 3 == 0) {
        category = "Пятиминутка"
        title = "Сбор рабочей группы на сегодня"
        description =
            "Соберите вашу команду в теплом месте и проведите инструктаж на день, озвучте стоп листы, пуш листы. мотивируйте сотрудников."
      }

      val crime = ChecklistModel(
          i,
          category,
          title,
          time,
          description
      )
      mChecklistModels.add(crime)
    }
  }

  fun getChecklists() = mChecklistModels

}
