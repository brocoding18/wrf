package com.gstudio.danil.myapplication.ui.working_day_log.data_source

import com.gstudio.danil.myapplication.api.dto.WorkingDayEventDTO
import com.gstudio.danil.myapplication.utils.ext.formattedDate
import com.gstudio.danil.myapplication.utils.ext.formattedTime

data class WorkingDayStoryItem(
  val id: Int,
  val _date: String?,
  val person: String?,
  val _time: String?,
  val type: Int = 0 // Для предыдущих смен
) : WorkingDayItem {
  override fun getItemId(): Int = id
  override fun getItemType(): Int = type

  var date: String? = formattedDate(_date)
  var time: String? = formattedTime(_time)

}

data class WorkingDayTemperatureItem(
  val id: Int,
  val type: Int = 1 // Для температуры
) : WorkingDayItem {
  override fun getItemId(): Int = id
  override fun getItemType(): Int = type
}

data class WorkingDayNotebookItem(
  val id: Int,
  val timeAndPerson: String,
  var text: String,
  val type: Int = 2 // Для событий текущей смены
) : WorkingDayItem {
  override fun getItemId(): Int = id
  override fun getItemType(): Int = type
}

interface WorkingDayItem {
  fun getItemId(): Int
  fun getItemType(): Int
}

object WorkingDayLab {

  lateinit var workingDayEvents: ArrayList<WorkingDayEventDTO>
  lateinit var workingDayFullList: ArrayList<WorkingDayItem>
  var workingDaySearchResult = ArrayList<WorkingDayItem>()

  fun getTemperaturePos() : Int {
    workingDayFullList.forEachIndexed { index, workingDayItem ->
      if (workingDayItem.getItemType() == 1) return index
    }

    return Int.MIN_VALUE
  }
}