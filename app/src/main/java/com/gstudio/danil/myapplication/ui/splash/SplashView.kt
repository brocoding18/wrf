package com.gstudio.danil.myapplication.ui.splash

import com.gstudio.danil.myapplication.mvp.BaseView

interface SplashView : BaseView {
  fun toLogin()
}