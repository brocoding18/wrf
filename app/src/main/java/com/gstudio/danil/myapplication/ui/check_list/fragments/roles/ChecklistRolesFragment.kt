package com.gstudio.danil.myapplication.ui.check_list.fragments.roles

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.Constants.ChecklistRolesData.checklistRoles
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.R.dimen
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.RecyclerItemDecorator
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter.OnItemClickListener
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import javax.inject.Inject

class ChecklistRolesFragment : BaseFragment(), ChecklistRolesView {
  companion object {
    fun newInstance(): ChecklistRolesFragment = ChecklistRolesFragment()
  }

  private lateinit var options: List<Category>

  @BindView(R.id.util_general_header) lateinit var headerText: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetailText: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout
  @BindView(R.id.util_toolbar_right_side_text) lateinit var headerRightSideText: TextView

  @BindView(R.id.util_fragment_recycler) lateinit var recyclerView: RecyclerView

  @BindView(R.id.util_fragment_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: ChecklistRolesPresenterImp

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_header_and_recycler_view)
    ButterKnife.bind(this, view)

    setupToolbar()

    return view
  }

  override fun onResume() {
    super.onResume()
    presenter.getRolesList()
  }

  override fun setupAdapter() {
    options= checklistRoles.map { it -> Category(it.id, it.name) }

    val currentAdapter = SingleTextViewAdapter(options, object : OnItemClickListener {
      override fun onItemClick(roleItem: Category) {
        presenter.revealTimetableScreen(roleItem.id, roleItem.name)
      }
    })

    recyclerView.apply {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(activity!!)
      addItemDecoration(
        RecyclerItemDecorator(resources.getDimension(dimen.general_recycler_bottom_padding))
      )
    }

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(recyclerView))
  }

  private fun setupToolbar() {
    headerText.text = "Чеклисты"
    headerDetailText.text = "Выберите ваш чеклист на сегодня"

    headerBackButton.setOnClickListener { onUpButtonClick() }
    headerRightSideText.setOnClickListener { presenter.revealProfile() }
  }
}