package com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event

import com.gstudio.danil.myapplication.mvp.BaseView

interface AddView : BaseView {
  fun onEventSent()
}