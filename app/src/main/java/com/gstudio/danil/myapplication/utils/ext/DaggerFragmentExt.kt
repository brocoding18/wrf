package com.gstudio.danil.myapplication.utils.ext

import android.content.Context
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import dagger.android.support.DaggerFragment

fun DaggerFragment.onUpButtonClick() {
  activity!!.onBackPressed()
}

fun DaggerFragment.hideKeyboard() {
  try {
    activity!!.window.setSoftInputMode(
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
    )
    if (activity!!.currentFocus != null && activity!!.currentFocus!!.windowToken != null) {
      (activity!!.getSystemService(
          Context.INPUT_METHOD_SERVICE
      ) as InputMethodManager).hideSoftInputFromWindow(
          activity!!.currentFocus!!.windowToken, 0
      )
    }
  } catch (e: Exception) {
    e.printStackTrace()
  }
}