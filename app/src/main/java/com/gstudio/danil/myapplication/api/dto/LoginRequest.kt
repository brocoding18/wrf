package com.gstudio.danil.myapplication.api.dto

data class LoginRequest(
  val phone: String?,
  val password: String?
)