package com.gstudio.danil.myapplication.ui.innovations.di

import android.content.Context
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesPresenter
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesPresenterImp
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesView
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionPresenter
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionPresenterImp
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionView
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessagePresenter
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessagePresenterImp
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class InnovationsFragmentModule {

  @Provides
  @PerFragment
  fun provideInnovationsHelloByeFragmentView(
    innovationsHelloByeFragment: MessageFragment
  ): MessageView =
    innovationsHelloByeFragment

  @Provides
  @PerFragment
  fun provideInnovationsCategoriesFragmentView(
    innovationsCategoriesFragment: CategoriesFragment
  ): CategoriesView =
    innovationsCategoriesFragment

  @Provides
  @PerFragment
  fun provideInnovationsDescriptionFragmentView(
    innovationsDescriptionFragment: DescriptionFragment
  ): DescriptionView =
    innovationsDescriptionFragment

  @Provides
  @PerFragment
  fun provideCategoriesPresenter(
    view: CategoriesView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): CategoriesPresenter =
    CategoriesPresenterImp(view, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideMessagePresenter(
    view: MessageView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): MessagePresenter =
    MessagePresenterImp(view, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideDescriptionPresenter(
    context: Context,
    view: DescriptionView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): DescriptionPresenter =
    DescriptionPresenterImp(context, view, apiService, router, spHelper)

}