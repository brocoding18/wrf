package com.gstudio.danil.myapplication.di.component

import com.gstudio.danil.myapplication.WrfApp
import com.gstudio.danil.myapplication.di.ActivityBuilder
import com.gstudio.danil.myapplication.di.PerApplication
import com.gstudio.danil.myapplication.di.module.ApiModule
import com.gstudio.danil.myapplication.di.module.AppModule
import com.gstudio.danil.myapplication.di.module.OkHttpModule
import com.gstudio.danil.myapplication.di.module.RetrofitModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@PerApplication
@Component(
    modules = [
      AndroidSupportInjectionModule::class,
      AppModule::class,
      ActivityBuilder::class,

      ApiModule::class,
      RetrofitModule::class,
      OkHttpModule::class
    ]
)
interface AppComponent : AndroidInjector<WrfApp> {
  @Component.Builder
  abstract class Builder : AndroidInjector.Builder<WrfApp>()

}
