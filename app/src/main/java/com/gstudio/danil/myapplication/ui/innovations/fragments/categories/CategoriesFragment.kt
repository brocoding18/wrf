package com.gstudio.danil.myapplication.ui.innovations.fragments.categories

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.Constants.InnovationsThemesData.innovationsThemes
import com.gstudio.danil.myapplication.Constants.Strings.INNOVATIONS_CATEGORIES_HEADER
import com.gstudio.danil.myapplication.Constants.Strings.INNOVATIONS_CATEGORIES_HEADER_DETAIL
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter.OnItemClickListener
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import javax.inject.Inject

class CategoriesFragment : BaseFragment(), CategoriesView {
  companion object {
    fun newInstance(): CategoriesFragment = CategoriesFragment()
  }

  @BindView(R.id.util_general_header) lateinit var themesHeader: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var themesHeaderDetail: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout
  @BindView(R.id.util_toolbar_right_side_text) lateinit var headerRightSideText: TextView
  @BindView(R.id.util_fragment_recycler) lateinit var recyclerView: RecyclerView

  @BindView(R.id.util_fragment_progress_bar) lateinit var progressBar: ProgressBar

  private lateinit var options: List<Category>

  @Inject lateinit var presenter: CategoriesPresenterImp

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_header_and_recycler_view)
    ButterKnife.bind(this, view)

    setupHeader()

    return view
  }

  override fun onResume() {
    super.onResume()

    presenter.fetchCategories()
  }

  override fun setupAdapter() {
    options = innovationsThemes.map { it -> Category(it.id, it.name) }

    val currentAdapter = SingleTextViewAdapter(options, object : OnItemClickListener {
      override fun onItemClick(optionsItem: Category) {
        presenter.onCategoryClicked(optionsItem.id)
      }
    })

    recyclerView.apply {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(activity!!)
    }

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(recyclerView))
  }

  private fun setupHeader() {
    themesHeader.text = INNOVATIONS_CATEGORIES_HEADER
    themesHeaderDetail.text = INNOVATIONS_CATEGORIES_HEADER_DETAIL

    headerBackButton.setOnClickListener { onUpButtonClick() }
    headerRightSideText.setOnClickListener { presenter.revealProfile() }
  }
}