package com.gstudio.danil.myapplication.ui.working_day_log.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayActivity
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayPresenter
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayPresenterImp
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class WorkingDayActivityModule {

  @Provides
  @PerActivity
  fun provideWorkingDayView(workingDayActivity: WorkingDayActivity): WorkingDayView = workingDayActivity

  @Provides
  @PerActivity
  fun provideWorkingDayNavigator(activity: WorkingDayActivity): SupportAppNavigator = SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun provideWorkingDayPresenter(
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): WorkingDayPresenter = WorkingDayPresenterImp(apiService, router, spHelper)

}