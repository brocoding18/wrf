package com.gstudio.danil.myapplication.ui.check_list.fragments.roles

import com.gstudio.danil.myapplication.Constants.ChecklistRolesData
import com.gstudio.danil.myapplication.Screens.ChecklistTimetableScreen
import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.ChecklistRolesDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ChecklistRolesPresenterImp @Inject constructor(
  private val rolesView: ChecklistRolesView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<ChecklistRolesView>(router), ChecklistRolesPresenter {
  override fun getRolesList() {
    disposable.add(
      apiService.getChecklistsRoles()
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: ChecklistRolesDTO ->
            ChecklistRolesData.checklistRoles = response.checklistRoles

            rolesView.setupAdapter()
          },
            { e: Throwable ->
              val errorBody = (e as? HttpException)?.response()
                  ?.errorBody()
              println(errorBody)
            }))
  }

  fun revealTimetableScreen(roleId: Int, roleName: String) {
    router.navigateTo(ChecklistTimetableScreen(roleId, roleName))
  }

  fun revealProfile() {
    router.navigateTo(ProfileActivityScreen)
  }
}