package com.gstudio.danil.myapplication.ui.innovations.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.innovations.InnovationsActivity
import com.gstudio.danil.myapplication.ui.innovations.InnovationsPresenter
import com.gstudio.danil.myapplication.ui.innovations.InnovationsPresenterImp
import com.gstudio.danil.myapplication.ui.innovations.InnovationsView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class InnovationsActivityModule {

  @Provides
  @PerActivity
  fun provideInnovationsView(innovationsActivity: InnovationsActivity): InnovationsView =
    innovationsActivity

  @Provides
  @PerActivity
  fun provideInnovationsNavigator(activity: InnovationsActivity): SupportAppNavigator = SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun provideInnovationsPresenter(
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): InnovationsPresenter = InnovationsPresenterImp(apiService, router, spHelper)

}
