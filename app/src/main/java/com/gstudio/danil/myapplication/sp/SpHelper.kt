package com.gstudio.danil.myapplication.sp

import android.content.SharedPreferences
import com.gstudio.danil.myapplication.api.dto.LoginRequest

class SpHelper(var sharedPreferences: SharedPreferences) {

  companion object {
    const val TOKEN = "token"
    const val USER_PHONE = "userPhone"
    const val USER_PASSWORD = "userPassword"
    const val PHOTO_PATH = "photo_path"
  }

  private var editor: SharedPreferences.Editor = sharedPreferences.edit()

  @Synchronized
  fun setToken(key: String, token: String) =
    editor.apply {
      putString(key, token)
      commit()
    }

  @Synchronized
  fun getToken(): String? = sharedPreferences.getString(TOKEN, null)


  @Synchronized
  fun setUserCredentials(credentials: HashMap<String, String>) =
    editor.apply {
      putString(USER_PHONE, credentials["userPhone"])
      putString(USER_PASSWORD, credentials["userPassword"])
      commit()
    }


  @Synchronized
  fun getUserCredentials(): LoginRequest {
    val userPhone = sharedPreferences.getString(USER_PHONE, null)
    val userPassword = sharedPreferences.getString(USER_PASSWORD, null)
    return LoginRequest(userPhone, userPassword)
  }

  @Synchronized
  fun setPhotoPath(photoPath: String?) =
      editor.apply {
        putString(PHOTO_PATH, photoPath)
        commit()
    }


  @Synchronized
  fun getPhotoPath(): String? = sharedPreferences.getString(PHOTO_PATH, null)
}