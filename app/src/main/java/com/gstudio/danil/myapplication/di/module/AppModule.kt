package com.gstudio.danil.myapplication.di.module

import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.net.ConnectivityManager
import android.net.NetworkInfo
import com.gstudio.danil.myapplication.di.PerApplication
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.utils.AppSchedulerProvider
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.Router

@Module
class AppModule {

  @Provides
  @PerApplication
  fun provideApplicationContext(application: Application): Context = application.applicationContext

  @Provides
  @PerApplication
  fun provideResources(application: Application): Resources = application.resources

  @Provides
  @PerApplication
  fun provideNetworkInfo(context: Context): NetworkInfo {
    val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

    return connectivityManager.activeNetworkInfo

//    return activeNetworkInfo != null && activeNetworkInfo.isConnected
  }

  @Provides
  @PerApplication
  fun provideSharedPref(application: Application): SpHelper =
    SpHelper(application.getSharedPreferences("wrf:Sp", Context.MODE_PRIVATE))

  @PerApplication
  @Provides
  fun provideCicerone(): Cicerone<Router> = Cicerone.create()

  @Provides
  @PerApplication
  fun provideRouter(cicerone: Cicerone<Router>): Router = cicerone.router

  @Provides
  @PerApplication
  fun provideNavigationHolder(cicerone: Cicerone<Router>): NavigatorHolder = cicerone.navigatorHolder

  @Provides
  @PerApplication
  fun provideCompositeDisposable() = CompositeDisposable()

  @Provides
  @PerApplication
  fun provideSchedulerProvider() = AppSchedulerProvider()

}
