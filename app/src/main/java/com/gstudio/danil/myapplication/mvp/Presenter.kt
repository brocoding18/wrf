package com.gstudio.danil.myapplication.mvp

interface Presenter<V : BaseView> {
  fun attachView(view: V)
  fun detachView()
}