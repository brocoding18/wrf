package com.gstudio.danil.myapplication.ui.check_list.fragments.checklists

import com.gstudio.danil.myapplication.mvp.BaseView

interface ChecklistFragmentView : BaseView {
  fun setupAdapter()
  fun showNotInTimeMessage()
//  fun paintStatus(status: String, position: Int)
}