package com.gstudio.danil.myapplication.ui.working_day_log.data_source.notebook

import android.arch.paging.PageKeyedDataSource
import android.content.res.Resources
import com.gstudio.danil.myapplication.Constants.WorkingDay.TOTAL_PAGES
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.WorkingDayPreviousListsDTO
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.*
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookView
import com.gstudio.danil.myapplication.utils.ext.formattedTime
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class WorkingDayDataSource @Inject constructor(
  val workingDayView: NotebookView,
  val resources: Resources,
  val apiService: Endpoints,
  val disposable: CompositeDisposable
) : PageKeyedDataSource<Int, WorkingDayItem>() {

  // TODO нужен рефакторинг

  private lateinit var newsResponse: Single<WorkingDayPreviousListsDTO>

  override fun loadInitial(
    params: PageKeyedDataSource.LoadInitialParams<Int>,
    callback: LoadInitialCallback<Int, WorkingDayItem>
  ) {
    newsResponse = createObservable(params, null)

    disposable.add(newsResponse.subscribe({ response: WorkingDayPreviousListsDTO ->
      val result: MutableList<WorkingDayItem> = ArrayList()

      // Запишем общее число страниц в базе
      TOTAL_PAGES = response.pagesTotal

      response.workingDayPrevious.forEach {
        // Нужно добавлять по 0-му инкдексу, потому что список нужен перевернутым.
        // "Пагинация при свайпе вверх"
        result.add(0,
          WorkingDayStoryItem(
            id = it.id,
            _date = it.createdAt,
            person = it.userOpened.name,
            _time = it.createdAt
          )
        )
      }

      result.add(
        WorkingDayTemperatureItem(
          id = -666
        )
      )

      WorkingDayLab.workingDayEvents.forEach { it ->
        val timeAndPerson = resources.getString(R.string.working_day_item_time_person, formattedTime(it.createdAt), it.user.name)

        val item =
          WorkingDayNotebookItem(
            id = it.id,
            timeAndPerson = timeAndPerson,
            text = it.eventText
          )

        result.add(item)
      }

      // Добавим результат init- запроса в общий список журнала смены, чтобы знать его размер
      WorkingDayLab.workingDayFullList = ArrayList(result)

      callback.onResult(result, 2, null)
    },
        { e: Throwable -> }))
  }

  // В этом методе мы даже не должны работать
  override fun loadAfter(
    params: PageKeyedDataSource.LoadParams<Int>,
    callback: LoadCallback<Int, WorkingDayItem>
  ) {
    if (params.key < 1 || params.key == null) {
      callback.onResult(ArrayList(), null)
      return
    }
    newsResponse = createObservable(null, params)
  }

  override fun loadBefore(
    params: PageKeyedDataSource.LoadParams<Int>,
    callback: LoadCallback<Int, WorkingDayItem>
  ) {
    if (params.key > TOTAL_PAGES || params.key == null) {
      // Перед оканчательным выходом из инструмента пагинации нужно очистить Disposable
      disposable.clear()
      callback.onResult(ArrayList(), null)
      return
    }
    newsResponse = createObservable(null, params)

    disposable.add(newsResponse.subscribe({ response: WorkingDayPreviousListsDTO ->
      val result: MutableList<WorkingDayItem> = ArrayList()

      response.workingDayPrevious.forEach {
        result.add(0,
          WorkingDayStoryItem(
            id = it.id,
            _date = it.closedAt,
            person = it.userOpened.name,
            _time = it.closedAt
          )
        )
      }

      val key =  params.key + 1
      // Добавим результат запроса пагинации в общий список журнала смены, чтобы знать его размер
      result.forEach {
        WorkingDayLab.workingDayFullList.add(0, it)
      }

      callback.onResult(result, key)
    }, { e: Throwable -> }))
  }

  private fun createObservable(
    loadInitialParams: PageKeyedDataSource.LoadInitialParams<Int>?,
    loadRangeParams: PageKeyedDataSource.LoadParams<Int>?
  ): Single<WorkingDayPreviousListsDTO> =
    if (loadInitialParams != null) {
      createInitialDataRequest()
    } else {
      createPaginationRequest(loadRangeParams!!.key)
    }

  private fun createInitialDataRequest(): Single<WorkingDayPreviousListsDTO> =
    apiService.getWorkingDayPreviousLists(1)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())

  private fun createPaginationRequest(key: Int): Single<WorkingDayPreviousListsDTO> =
    apiService.getWorkingDayPreviousLists(key)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
}