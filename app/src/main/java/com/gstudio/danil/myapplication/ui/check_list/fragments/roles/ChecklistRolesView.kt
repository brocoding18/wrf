package com.gstudio.danil.myapplication.ui.check_list.fragments.roles

import com.gstudio.danil.myapplication.mvp.BaseView

interface ChecklistRolesView : BaseView {
  fun setupAdapter()
}