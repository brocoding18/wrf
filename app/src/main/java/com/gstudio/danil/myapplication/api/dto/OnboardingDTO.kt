package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class HelloOnboardingDTO(
  @SerializedName("data") val data: List<HelloOnboarding>
)

data class HelloOnboarding(
  @SerializedName("id") val id: Int,
  @SerializedName("title") val title: String,
  @SerializedName("text") val helloText: String,
  @SerializedName("image_url") val imageUrl: String?
)

data class OnboardingDTO(
  @SerializedName("text") val text: String
)