package com.gstudio.danil.myapplication.ui.main_menu.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.main_menu.MenuActivity
import com.gstudio.danil.myapplication.ui.main_menu.MenuPresenter
import com.gstudio.danil.myapplication.ui.main_menu.MenuPresenterImp
import com.gstudio.danil.myapplication.ui.main_menu.MenuView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class MenuActivityModule {

  @Provides
  @PerActivity
  fun provideMenuView(menuActivity: MenuActivity): MenuView = menuActivity

  @Provides
  @PerActivity
  fun provideMenuNavigator(activity: MenuActivity): SupportAppNavigator = SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun provideMenuPresenter(
    menuView: MenuView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): MenuPresenter = MenuPresenterImp(menuView, apiService, router, spHelper)

}