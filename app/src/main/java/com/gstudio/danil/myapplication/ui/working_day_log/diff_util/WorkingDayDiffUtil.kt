package com.gstudio.danil.myapplication.ui.working_day_log.diff_util

import android.support.v7.util.DiffUtil
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem

object WorkingDayDiffUtil {
  var diffCallback: DiffUtil.ItemCallback<WorkingDayItem> = object : DiffUtil.ItemCallback<WorkingDayItem>() {
    override fun areItemsTheSame(oldItem: WorkingDayItem, newItem: WorkingDayItem): Boolean =
      oldItem.getItemId() == oldItem.getItemId()

    override fun areContentsTheSame(oldItem: WorkingDayItem, newItem: WorkingDayItem): Boolean =
        oldItem === newItem
  }
}