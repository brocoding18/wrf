package com.gstudio.danil.myapplication.ui.profile.fragment

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.glide.GlideApp
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import javax.inject.Inject

class ProfileFragment : BaseFragment(), ProfileFragmentView {
  companion object {
    fun newInstance(): ProfileFragment = ProfileFragment()
  }

  @BindView(R.id.util_general_header) lateinit var header: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout
  @BindView(R.id.util_toolbar_right_side_container) lateinit var headerRightSide: ConstraintLayout

  @BindView(R.id.profile_content_container) lateinit var contentContainer: ConstraintLayout
  @BindView(R.id.profile_user_name) lateinit var userNameView: TextView
  @BindView(R.id.profile_place_and_function) lateinit var placeOfWorkAndFunctionView: TextView
  @BindView(R.id.textView4) lateinit var checklistsView: TextView
  @BindView(R.id.profile_avatar) lateinit var avatarView: ImageView

  @BindView(R.id.profile_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: ProfileFragmentPresenterImp

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.profile_layout)
    ButterKnife.bind(this, view)

    hideAndShowViews(viewsToGone = listOf(headerRightSide))
    header.text = "Профиль"
    headerDetail.text = "Здесь находятся основная информация и настройки вашего профиля"
    headerBackButton.setOnClickListener { onUpButtonClick() }

    presenter.getProfileData()

    return view
  }

  override fun onResume() {
    super.onResume()
  }

  override fun putDataToViews(
    // TODO убрать non-null
    userName: String?,
    imageUrl: String?,
    restaurantName: String?,
    function: String?,
    checklistsDone: Int,
    checklistsTotal: Int
  ) {
    userNameView.text = userName
    placeOfWorkAndFunctionView.text = resources.getString(R.string.profile_place_and_function, restaurantName, function)
    checklistsView.text = resources.getString(R.string.profile_checklists, checklistsDone, checklistsTotal)
    GlideApp.with(this)
        .load(imageUrl)
        .placeholder(R.drawable.rabbit_placeholder)
        .into(avatarView)

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(contentContainer))
  }
}