package com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook

import com.gstudio.danil.myapplication.mvp.BaseView

interface NotebookView : BaseView{
  fun setupNotebookAdapter()
  fun scrollToTempPos()

  fun notifySearchResults()
}