package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class WorkingDayEventsDTO(
  @SerializedName("data") val workingDayEvents: ArrayList<WorkingDayEventDTO>,
  @SerializedName("pages") val pages: Int
)

data class WorkingDayEventDTO(
  @SerializedName("event_text") val eventText: String,
  @SerializedName("id") val id: Int,
  @SerializedName("created_at") val createdAt: String,
  @SerializedName("user") val user: WorkingDayUser
)

data class WorkingDayFinishDTO(
  @SerializedName("id") val id: Int,
  @SerializedName("user_opened") val userOpened: WorkingDayUser,
  @SerializedName("user_closed") val userClosed: WorkingDayUser
)

data class WorkingDayUser(
  @SerializedName("id") val id: Int?,
  @SerializedName("name") val name: String?
)