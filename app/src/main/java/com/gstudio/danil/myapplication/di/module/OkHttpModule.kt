package com.gstudio.danil.myapplication.di.module

import android.app.Application
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.gstudio.danil.myapplication.api.ApiServiceHolder
import com.gstudio.danil.myapplication.api.TokenAuthenticator
import com.gstudio.danil.myapplication.api.interceptors.CachingControlInterceptor
import com.gstudio.danil.myapplication.di.PerApplication
import com.gstudio.danil.myapplication.sp.SpHelper
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.Dispatcher
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

@Module
class OkHttpModule {

  @Provides
  @PerApplication
  fun providerBaseBuilder(): OkHttpClient.Builder = OkHttpClient.Builder()
      .retryOnConnectionFailure(true)
      .connectTimeout(360, TimeUnit.SECONDS)
      .readTimeout(360, TimeUnit.SECONDS)
      .writeTimeout(360, TimeUnit.SECONDS)

  @Provides
  @PerApplication
  fun provideOkHttpCache(application: Application): Cache =
    Cache(application.cacheDir, 10 * 1024 * 1024)

  @Provides
  @PerApplication
  fun provideLoggingInterceptor(): HttpLoggingInterceptor {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    return interceptor
  }

  @Provides
  @PerApplication
  fun provideAuthenticator(
    spHelper: SpHelper,
    apiServiceHolder: ApiServiceHolder
  ): TokenAuthenticator = TokenAuthenticator(spHelper, apiServiceHolder)

  @Provides
  @PerApplication
  fun provideCachingInterceptor(spHelper: SpHelper): CachingControlInterceptor = CachingControlInterceptor(spHelper)

  @Provides
  @PerApplication
  fun provideDispatcher(): Dispatcher = Dispatcher().apply { maxRequests = 3 }

  @Provides
  @PerApplication
  fun provideOkHttp(
    builder: OkHttpClient.Builder,
    cache: Cache,
    loggingInterceptor: HttpLoggingInterceptor,
    tokenAuthenticator: TokenAuthenticator,
    cachingControlInterceptor: CachingControlInterceptor,
    dispatcher: Dispatcher
  ): OkHttpClient = builder
      .cache(cache)
      .addNetworkInterceptor(StethoInterceptor())
      .addNetworkInterceptor(cachingControlInterceptor)
      .addInterceptor(loggingInterceptor)
      .authenticator(tokenAuthenticator)
      .dispatcher(dispatcher)
      .build()

}

