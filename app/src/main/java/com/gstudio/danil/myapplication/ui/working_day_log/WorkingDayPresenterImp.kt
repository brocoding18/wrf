package com.gstudio.danil.myapplication.ui.working_day_log

import com.gstudio.danil.myapplication.Screens
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class WorkingDayPresenterImp @Inject constructor(
  private val apiServce: Endpoints,
  private val router: Router,
  private val spHelper: SpHelper
) : BasePresenter<WorkingDayView>(router), WorkingDayPresenter {

  fun notebookScreen() {
    router.newRootScreen(Screens.WorkingDayNotebook)
  }
}