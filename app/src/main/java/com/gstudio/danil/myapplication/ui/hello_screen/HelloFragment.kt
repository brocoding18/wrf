package com.gstudio.danil.myapplication.ui.hello_screen

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.TempHelloMessage
import com.gstudio.danil.myapplication.ui.main_menu.MenuActivity
import com.gstudio.danil.myapplication.utils.ext.inflate

class HelloFragment : Fragment() {
  companion object {
    var isLastPage = false
    private val TAG = HelloFragment::class.java.simpleName
    private const val ARG_HELLO_MESSAGE = "hello_message"

    fun newInstance(helloMessage: TempHelloMessage): HelloFragment {
      val args = Bundle()
      args.putParcelable(ARG_HELLO_MESSAGE, helloMessage)

      val fragment = HelloFragment()
      fragment.arguments = args
      return fragment
    }
  }

  private val MAIN_HELLO_MESSAGE_SIZE = 45F
  private val FIRST_PAGE = 1

  private lateinit var mHelloMessage: TempHelloMessage
  private lateinit var mViewPager: ViewPager

  @BindView(R.id.hello_main_message) lateinit var mHelloMainMessage : TextView
  @BindView(R.id.hello_top_message) lateinit var mHelloTopMessage : TextView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    mHelloMessage = arguments!!.getParcelable(ARG_HELLO_MESSAGE)!!
    mViewPager = (activity as HelloActivity).viewPager
  }

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.hello_screen_fragment)
    ButterKnife.bind(this, view)

    if (mHelloMessage.id == FIRST_PAGE) {
      mHelloMainMessage.textSize = MAIN_HELLO_MESSAGE_SIZE
    }

    mHelloTopMessage.text = mHelloMessage.topMessage
    mHelloMainMessage.text = mHelloMessage.middleMessage

    return view
  }

  @OnClick(R.id.hello_fragment_container)
  fun onLayoutClick() {
    mViewPager.setCurrentItem(mViewPager.currentItem + 1, true)
    if (isLastPage) {
      val intent = MenuActivity.newIntent(activity!!)
      startActivity(intent)
      activity!!.finish()
    }
  }
}