package com.gstudio.danil.myapplication.ui.check_list

interface ChecklistActivityPresenter {
  fun revealRolesScreen()
}