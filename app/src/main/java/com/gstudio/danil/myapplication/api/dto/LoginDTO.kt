package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class LoginDTO (
  @SerializedName("token") val token: String
)