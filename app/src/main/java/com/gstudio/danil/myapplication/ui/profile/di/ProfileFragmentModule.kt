package com.gstudio.danil.myapplication.ui.profile.di

import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragment
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragmentPresenter
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragmentPresenterImp
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragmentView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ProfileFragmentModule {

  @Provides
  @PerFragment
  fun provideProfileFragmentView(
    profileFragment: ProfileFragment
  ): ProfileFragmentView = profileFragment

  @Provides
  @PerFragment
  fun provideProfileFragmentPresenter(
    profileFragmentView: ProfileFragmentView,
      apiService: Endpoints,
      router: Router
  ): ProfileFragmentPresenter = ProfileFragmentPresenterImp(profileFragmentView, apiService, router)


}