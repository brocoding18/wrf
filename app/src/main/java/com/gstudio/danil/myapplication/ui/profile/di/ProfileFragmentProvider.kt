package com.gstudio.danil.myapplication.ui.profile.di

import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ProfileFragmentProvider {

  @PerFragment
  @ContributesAndroidInjector(modules = [ProfileFragmentModule::class])
  abstract fun provideProfileFragmentFactory(): ProfileFragment

}