package com.gstudio.danil.myapplication

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TempHelloMessage(
  val id: Int,
  val topMessage: String?,
  val middleMessage: String,
  val bottomMessage: String? = null
) : Parcelable
