package com.gstudio.danil.myapplication.ui.login

import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.LoginDTO
import com.gstudio.danil.myapplication.api.dto.LoginRequest
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.sp.SpHelper.Companion.TOKEN
import com.gstudio.danil.myapplication.sp.SpHelper.Companion.USER_PASSWORD
import com.gstudio.danil.myapplication.sp.SpHelper.Companion.USER_PHONE
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.json.JSONException
import org.json.JSONObject
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import java.io.IOException
import javax.inject.Inject

class LoginPresenterImp @Inject constructor(
  private val loginView: LoginView,
  private val api: Endpoints,
  private val router: Router,
  private val spHelper: SpHelper
) : BasePresenter<LoginView>(router), LoginPresenter {

  override fun login(
    phone: String,
    password: String
  ) {
    disposable.add(
        api.postUser(LoginRequest(phone, password))
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response: LoginDTO ->
                  spHelper.setUserCredentials(
                      hashMapOf(USER_PHONE to phone, USER_PASSWORD to password)
                  )

                  spHelper.setToken(TOKEN, response.token)
                  loginView.onLoginSuccessful()
                },
                { e: Throwable ->
                  val errorBody = (e as HttpException).response()
                      .errorBody()
                  val error: JSONObject

                  try {
                    error = JSONObject(errorBody!!.string())

                    loginView.showWrongPassword(error.getString("message"))
                  } catch (e1: JSONException) {
                    e1.printStackTrace()
                  } catch (e1: IOException) {
                    e1.printStackTrace()
                  }
                })
    )
  }
}