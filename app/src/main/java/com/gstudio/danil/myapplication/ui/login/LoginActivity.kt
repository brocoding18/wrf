package com.gstudio.danil.myapplication.ui.login

import android.Manifest
import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.transition.ChangeBounds
import android.transition.Transition
import android.util.Log
import android.view.animation.DecelerateInterpolator
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.daimajia.androidanimations.library.Techniques.Shake
import com.daimajia.androidanimations.library.YoYo
import com.gstudio.danil.myapplication.Constants.Strings.PHONE_MASK
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import com.gstudio.danil.myapplication.ui.hello_screen.HelloActivity
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.hideKeyboard
import com.gstudio.danil.myapplication.utils.ext.showSnackbarWithMessage
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.MaskedTextChangedListener.ValueListener
import kotlinx.android.synthetic.main.login_activity.*
import permissions.dispatcher.NeedsPermission
import permissions.dispatcher.OnPermissionDenied
import permissions.dispatcher.RuntimePermissions
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

@RuntimePermissions
class LoginActivity : BaseActivity(), LoginView {
  companion object {
    private val LAYOUT = R.layout.login_activity
    private val LAYOUT_CONTAINTER = R.id.login_container
    private val TAG = LoginActivity::class.java.simpleName
    const val VIEW_LOGO_HEADER_IMAGE = "detail:headerText:image"
  }

  @BindView(R.id.user_phone_number) lateinit var userPhone: EditText
  @BindView(R.id.user_phone_number_text) lateinit var userPhoneText: TextView
  @BindView(R.id.user_password) lateinit var userPassword: EditText
  @BindView(R.id.user_password_text) lateinit var userPasswordText: TextView
  @BindView(R.id.login_button) lateinit var loginButton: Button

  @BindView(R.id.login_scroll) lateinit var loginContainer: ScrollView
  @BindView(R.id.login_progress_bar) lateinit var progressBar: ProgressBar

  private lateinit var editListener: MaskedTextChangedListener

  @Inject lateinit var presenter: LoginPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(LAYOUT)
    ButterKnife.bind(this)

    bindMask()

    ViewCompat.setTransitionName(login_top_logo, VIEW_LOGO_HEADER_IMAGE)
    window.sharedElementEnterTransition = enterTransition()
  }

  override fun onResume() {
    super.onResume()
  }

  @SuppressLint("NeedOnRequestPermissionsResult")
  override fun onRequestPermissionsResult(
    requestCode: Int,
    permissions: Array<out String>,
    grantResults: IntArray
  ) {
    super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    onRequestPermissionsResult(requestCode, grantResults)
  }

  private fun enterTransition(): Transition = ChangeBounds().apply {
    duration = 500
    interpolator = DecelerateInterpolator()
  }

  @OnClick(R.id.login_button)
  fun login() {
    hideKeyboard()
    hideAndShowViews(viewsToHide = listOf(loginContainer), viewsToShow = listOf(progressBar))

    val phone = phoneFormat()

    presenter.login(phone, userPassword.text.toString())
  }

  override fun onLoginSuccessful() {
    showHelloScreenWithPermissionCheck()
  }

  // TODO использовать Чичерон
  // TODO перевести навигацию в Презентер
  @NeedsPermission(
      Manifest.permission.CAMERA,
      Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.WRITE_EXTERNAL_STORAGE
  )
  override fun showHelloScreen() {
    val intent = HelloActivity.newIntent(this)
    startActivity(intent)
    finish()
  }

  @OnPermissionDenied(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE,
      Manifest.permission.WRITE_EXTERNAL_STORAGE)
  fun onPermissionDenied() {
    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(loginContainer))
    animateLoginContainerElements()
    showSnackbarWithMessage(this, LAYOUT_CONTAINTER, "Вы не предоставили приложению необходимый доступ к телефону")
  }

  override fun bindMask() {
    editListener = MaskedTextChangedListener(PHONE_MASK, userPhone, object : ValueListener {
      override fun onTextChanged(
        maskFilled: Boolean,
        extractedValue: String
      ) {
        Log.d(TAG, extractedValue)
        Log.d(TAG, maskFilled.toString())
      }
    })

    userPhone.apply {
      addTextChangedListener(editListener)
      onFocusChangeListener = editListener
    }
  }

  private fun phoneFormat() =
    userPhone.text.toString().replace("\\s".toRegex(), "").replace("\\(".toRegex(), "").replace(
      "\\)".toRegex(), ""
    )

  override fun showWrongPassword(string: String) {
    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(loginContainer))

    animateLoginContainerElements()

    showSnackbarWithMessage(this, LAYOUT_CONTAINTER, string)
  }

  private fun animateLoginContainerElements() {
    YoYo.with(Shake).duration(850).playOn(loginButton)
    YoYo.with(Shake).duration(850).playOn(userPhoneText)
    YoYo.with(Shake).duration(850).playOn(userPasswordText)
  }
}
