package com.gstudio.danil.myapplication.ui.innovations.fragments.description

import android.app.Activity.RESULT_OK
import android.content.ContentValues
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.constraint.ConstraintLayout
import android.support.v4.content.FileProvider
import android.telephony.MbmsDownloadSession.RESULT_CANCELLED
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.Constants.Strings.ATTACH_PHOTO
import com.gstudio.danil.myapplication.Constants.Strings.DETACH_PHOTO
import com.gstudio.danil.myapplication.Constants.Strings.INNOVATIONS_DESCRIPTION_HEADER
import com.gstudio.danil.myapplication.Constants.Strings.INNOVATIONS_DESCRIPTION_HEADER_DETAIL
import com.gstudio.danil.myapplication.Constants.Strings.INNOVATIONS_DESCRIPTION_SEND_BUTTON_TEXT
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.dto.InnovationsShareRequest
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import com.gstudio.danil.myapplication.utils.ext.hideKeyboard
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import com.gstudio.danil.myapplication.utils.ext.showKeyboard
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject

class DescriptionFragment : BaseFragment(), DescriptionView {
  companion object {
    private const val ARG_INNOVATIONS_CATEGORY_ID = "innovations_category_id"
    private const val REQUEST_TAKE_PHOTO = 1
    private const val PERMISSION_REQUEST_EXTERNAL_STORAGE = 10

    var canTakePhoto: Boolean = false

    fun newInstance(id: Int): DescriptionFragment {
      val args = Bundle()
      args.putInt(ARG_INNOVATIONS_CATEGORY_ID, id)

      val fragment = DescriptionFragment()
      fragment.arguments = args
      return fragment
    }
  }

  @BindView(R.id.util_general_header) lateinit var header: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar_right_side_text) lateinit var toolbarRightSideText: TextView
  @BindView(R.id.util_toolbar_right_side_icon) lateinit var toolbarRightSideIcon: ImageView
  @BindView(R.id.util_description_button) lateinit var sendButton: Button
  @BindView(R.id.util_description_message) lateinit var messageText: EditText
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout

  @Inject lateinit var presenter: DescriptionPresenterImp

  private var photoPath: String? = null

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_description_fragment)
    ButterKnife.bind(this, view)

    setupHeader()

    messageText.showKeyboard(activity!!)

    return view
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    when (requestCode) {
      REQUEST_TAKE_PHOTO -> {
        if (resultCode == RESULT_OK) {
          galleryAddPic()
          presenter.saveImagePath(photoPath!!)
          canTakePhoto = false
          updateRightSideToolbarContainer()
        } else if (resultCode == RESULT_CANCELLED) {
          // Do nothing?
        }
        messageText.showKeyboard(activity!!)
      }
    }
  }

  override fun onResume() {
    super.onResume()

    photoPath = presenter.getPhotoPath()

    canTakePhoto = photoPath == null
    updateRightSideToolbarContainer()
  }

  override fun onPause() {
    super.onPause()
    hideKeyboard()
  }

  private fun updateRightSideToolbarContainer() {
    if (canTakePhoto) {
      toolbarRightSideIcon.setImageResource(R.drawable.menu_icon_add)
      toolbarRightSideText.text = ATTACH_PHOTO
    } else {
      toolbarRightSideIcon.setImageResource(R.drawable.menu_icon_remove)
      toolbarRightSideText.text = DETACH_PHOTO
    }
  }

  @OnClick(R.id.util_description_button)
  override fun onShareButtonClick() {
    MessageFragment.isLastPage = true
    hideKeyboard()

    val id = arguments!!.getInt(ARG_INNOVATIONS_CATEGORY_ID)

    val innovationObject = InnovationsShareRequest(
        categoryId = id,
        ideaText = messageText.text.toString()
    )

    presenter.onShareButtonClick(innovationObject)
  }

  @OnClick(R.id.util_toolbar_right_side_container)
  override fun onAttachPhotoClick() {
    if (canTakePhoto) {
      dispatchTakePictureIntent()
    } else {
      photoPath = null
      presenter.deleteImagePath()
      canTakePhoto = true
      updateRightSideToolbarContainer()
    }
  }

  private fun setupHeader() {
    header.text = INNOVATIONS_DESCRIPTION_HEADER
    headerDetail.text = INNOVATIONS_DESCRIPTION_HEADER_DETAIL
    sendButton.text = INNOVATIONS_DESCRIPTION_SEND_BUTTON_TEXT

    headerBackButton.setOnClickListener { onUpButtonClick() }
  }

  private fun dispatchTakePictureIntent() {
    Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
      // Ensure that there's a camera activity to handle the intent
      takePictureIntent.resolveActivity(activity!!.packageManager)?.also {
        // Create the File where the photo should go
        val photoFile: File? = try {
          createImageFile()
        } catch (ex: IOException) {
          // Error occurred while creating the File
          null
        }
        // Continue only if the File was successfully created
        photoFile?.also {
          val photoURI: Uri = FileProvider.getUriForFile(
              activity!!,
              "com.gstudio.danil.myapplication.fileprovider",
              it
          )
          takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
          startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO)
        }
      }
    }
  }

  @Throws(IOException::class)
  private fun createImageFile(): File {
    // Create an image file name
    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
    val imageFileName = "JPEG_" + timeStamp + "_"
    val storageDir = activity!!.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

    return File.createTempFile(
        imageFileName, /* prefix */
        ".jpg", /* suffix */
        storageDir      /* directory */
    )
        .apply {
          // Save a file: path for use with ACTION_VIEW intents
          photoPath = absolutePath
        }
  }

  private fun galleryAddPic() {
    val values = ContentValues()
    values.put(MediaStore.Images.Media.DATA, photoPath)
    values.put(MediaStore.Images.Media.MIME_TYPE,"image/jpeg")
    activity!!.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,values)
  }
}
