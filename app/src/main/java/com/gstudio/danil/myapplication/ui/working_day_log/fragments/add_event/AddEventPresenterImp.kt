package com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event

import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventDTO
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventRequest
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class AddEventPresenterImp @Inject constructor(
  val addEventView: AddView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<AddView>(router), AddEventPresenter {

  fun addEvent(request: WorkingDayEventRequest) {
    disposable.add(
        apiService.postWorkingDayEvent(request)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: WorkingDayEventDTO ->
              addEventView.onEventSent()
            },
                { e: Throwable ->
                  val errorBody = (e as? HttpException)?.response()
                      ?.errorBody()
                  println(errorBody)
                })
    )
  }

  fun revealProfile() {
    router.navigateTo(ProfileActivityScreen)
  }

}