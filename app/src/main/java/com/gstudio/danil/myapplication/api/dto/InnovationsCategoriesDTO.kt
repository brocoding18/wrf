package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class InnovationsCategoriesDTO(
  @SerializedName("data") val categories: ArrayList<InnovationsCategories>
)

data class InnovationsCategories(
  @SerializedName("id") var id: Int,
  @SerializedName("name") var name: String
)