package com.gstudio.danil.myapplication.ui.login

interface LoginPresenter {
  fun login(phone: String, password: String)
}