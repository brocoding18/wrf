package com.gstudio.danil.myapplication.ui.innovations.fragments.categories

import com.gstudio.danil.myapplication.Constants.InnovationsThemesData
import com.gstudio.danil.myapplication.Screens.InnovationDescriptionScreen
import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.InnovationsCategoriesDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class CategoriesPresenterImp @Inject constructor(
  val categoriesView: CategoriesView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<CategoriesView>(router), CategoriesPresenter {

  fun fetchCategories() {
    disposable.add(
        apiService.getInnovationsCategories()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: InnovationsCategoriesDTO ->

              InnovationsThemesData.innovationsThemes = response.categories
              categoriesView.setupAdapter()
            },
                { e: Throwable ->
                  val errorBody = (e as? HttpException)?.response()
                      ?.errorBody()
                  println(errorBody)
                })
    )
  }

  fun onCategoryClicked(id: Int) {
    router.navigateTo(InnovationDescriptionScreen(id))
  }

  fun revealProfile() {
    router.navigateTo(ProfileActivityScreen)
  }
}