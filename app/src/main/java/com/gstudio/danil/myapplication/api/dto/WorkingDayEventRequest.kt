package com.gstudio.danil.myapplication.api.dto

data class WorkingDayEventRequest(
  val event_text: String
)