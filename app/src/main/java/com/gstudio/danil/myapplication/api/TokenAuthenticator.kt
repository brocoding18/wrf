package com.gstudio.danil.myapplication.api

import com.gstudio.danil.myapplication.Constants.Strings.AUTHORIZATION_HEADER
import com.gstudio.danil.myapplication.api.dto.LoginDTO
import com.gstudio.danil.myapplication.api.dto.RefreshRequest
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.sp.SpHelper.Companion.TOKEN
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route
import org.json.JSONObject

class TokenAuthenticator(
  private val spHelper: SpHelper,
  private val apiServiceHolder: ApiServiceHolder
) : Authenticator {

  override fun authenticate(
    route: Route?,
    response: Response
  ): Request? {
    if (apiServiceHolder.apiService == null) return null

    // Добавь вместо `val body = JSONObject(response.body()!!.string())`
    // `val body = JSONObject(response.peekBody(Long.MAX_VALUE).string())`
    // чтобы избежать ошибки closed

    val body = JSONObject(response.peekBody(Long.MAX_VALUE).string())
    val error = body.getString("error")
    val sessionToken = spHelper.getToken()

    if (error == "EarlyRefreshError" || error == "MissingTokenHeader") {
      return createRequest(response, sessionToken!!)
    }

    if (error == "ExpiredAccessError") {
      val refreshCall = apiServiceHolder.apiService!!.refreshToken(RefreshRequest(sessionToken!!))

      val refreshResponse: retrofit2.Response<LoginDTO> = refreshCall.execute()

      if (refreshResponse.code() == 200) {
        //read new JWT value from response body or interceptor depending upon your JWT availability logic
        val token = refreshResponse.body()!!.token
        spHelper.setToken(TOKEN, token)

        return createRequest(response, token)
      }
    }

    return null
  }

  private fun createRequest(response: Response, token: String): Request? = response.request()
        .newBuilder()
        .header(AUTHORIZATION_HEADER, "Bearer $token")
        .build()

}
