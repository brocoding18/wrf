package com.gstudio.danil.myapplication.ui.check_list.fragments.checklists

import com.gstudio.danil.myapplication.Constants
import com.gstudio.danil.myapplication.Constants.ChecklistsData.checklists
import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.Checklist
import com.gstudio.danil.myapplication.api.dto.ChecklistPatchRequest
import com.gstudio.danil.myapplication.api.dto.ChecklistsDTO
import com.gstudio.danil.myapplication.api.dto.UserChecklistStatus
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.check_list.adapter.ChecklistAdapter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

// TODO переименуй меня
class ChecklistPresenterImp @Inject constructor(
  private val checklistFragmentView: ChecklistFragmentView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<ChecklistFragmentView>(router), ChecklistPresenter {

  fun patchChecklist(
    itemId: Int,
    checklistHolder: ChecklistAdapter.CheckListHolder,
    status: ChecklistPatchRequest,
    position: Int
  ) {
    disposable.add(
        apiService.patchChecklist(itemId, status)
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: UserChecklistStatus ->

              val checklistForChanges: Checklist = checklists.find { it.id == itemId }!!
              checklistForChanges.userCheckList = response

              checklistHolder.changeStatusColorAndNotify(status.status, position)
            },
                { e: Throwable ->
                  val errorBody = (e as? HttpException)?.response()
                      ?.errorBody()
                  println(errorBody)
                })
    )
  }

  fun getChecklists(roleId: Int, timetableId: Int) {
    disposable.add(
      apiService.getChecklists(roleId, timetableId)
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: ChecklistsDTO ->
            Constants.ChecklistsData.checklists = response.data

            checklistFragmentView.setupAdapter()
          },
            { e: Throwable ->

              val errorBody = (e as? HttpException)?.response()
                  ?.errorBody()
              println(errorBody)
            })
    )
  }

  fun showNotInTimeMessage() {
    checklistFragmentView.showNotInTimeMessage()
  }

  fun revealProfile() {
    router.navigateTo(ProfileActivityScreen)
  }
}