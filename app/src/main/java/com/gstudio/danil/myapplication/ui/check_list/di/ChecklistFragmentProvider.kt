package com.gstudio.danil.myapplication.ui.check_list.di

import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetableFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ChecklistFragmentProvider {

  @PerFragment
  @ContributesAndroidInjector(modules = [ChecklistFragmentModule::class])
  abstract fun provideChecklistFragmentFactory(): ChecklistFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [ChecklistFragmentModule::class])
  abstract fun provideChecklistRolesFragmentFactory(): ChecklistRolesFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [ChecklistFragmentModule::class])
  abstract fun provideChecklistTimetableFragmentFactory(): ChecklistTimetableFragment

}