package com.gstudio.danil.myapplication.ui.hello_screen

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.gstudio.danil.myapplication.TempHelloMessage

class HelloPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {
  // Уйдет, когда будет БЭК
  private val messageStorage = arrayListOf(
      TempHelloMessage(1, null, "Доброго дня\nWhite Rabbit"),
      TempHelloMessage(
          2, "Ценности", "Тут нужно ввести мотивационный текст про фамели и ценности"
      ),
      TempHelloMessage(
          3, "Семья", "Пишем про дружбу и поддерку каждого члена семьи"
      )
  )

  override fun getItem(position: Int): Fragment {
    val message = messageStorage[position]
    return HelloFragment.newInstance(message)
  }

  override fun getCount(): Int = messageStorage.size
}