package com.gstudio.danil.myapplication.ui.login

import com.gstudio.danil.myapplication.mvp.BaseView

interface LoginView : BaseView {
  fun bindMask()
  fun onLoginSuccessful()
  fun showHelloScreen()
  fun showWrongPassword(string: String)
}