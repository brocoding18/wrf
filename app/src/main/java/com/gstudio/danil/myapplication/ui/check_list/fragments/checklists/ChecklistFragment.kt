package com.gstudio.danil.myapplication.ui.check_list.fragments.checklists

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.Constants.ChecklistsData.checklists
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.ui.check_list.adapter.ChecklistAdapter
import com.gstudio.danil.myapplication.ui.check_list.swipe.ItemTouchHelperCallback
import com.gstudio.danil.myapplication.utils.RecyclerItemDecorator
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import com.gstudio.danil.myapplication.utils.ext.showSnackbarWithMessage
import javax.inject.Inject

// TODO переименовать класс
class ChecklistFragment  : BaseFragment(), ChecklistFragmentView {
  companion object {
    private const val ARG_CHECKLIST_TIMETABLE_NAME = "checklist_screen_timetable_name"
    private const val ARG_CHECKLIST_TIMETABLE_ID = "checklist_screen_timetable_id"
    private const val ARG_CHECKLIST_ROLE_ID = "checklist_screen_role_id"

    fun newInstance(roleId: Int, timetableId: Int, timetableName: String): ChecklistFragment {
      val args = Bundle()
      args.putString(ARG_CHECKLIST_TIMETABLE_NAME, timetableName)
      args.putInt(ARG_CHECKLIST_TIMETABLE_ID, timetableId)
      args.putInt(ARG_CHECKLIST_ROLE_ID, roleId)

      val fragment = ChecklistFragment()
      fragment.arguments = args
      return fragment
    }
  }

  @BindView(R.id.util_general_header) lateinit var headerText: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout
  @BindView(R.id.util_toolbar_right_side_text) lateinit var headerRightSideText: TextView
  @BindView(R.id.util_fragment_recycler) lateinit var recyclerView: RecyclerView

  @BindView(R.id.util_fragment_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: ChecklistPresenterImp

  private lateinit var currentAdapter: ChecklistAdapter
  private var timetableName: String? = null
  private var timetableId: Int? = null
  private var roleId: Int? = null

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_header_and_recycler_view)
    ButterKnife.bind(this, view)

    unboxArguments()
    setupToolbar()

    return view
  }

  override fun onResume() {
    super.onResume()
    presenter.getChecklists(roleId!!, timetableId!!)
  }

  private fun unboxArguments() {
    timetableName = arguments!!.getString(ARG_CHECKLIST_TIMETABLE_NAME)
    timetableId = arguments!!.getInt(ARG_CHECKLIST_TIMETABLE_ID)
    roleId = arguments!!.getInt(ARG_CHECKLIST_ROLE_ID)
  }

  override fun setupAdapter() {
    currentAdapter = ChecklistAdapter(checklists, presenter)

    with(recyclerView) {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(activity!!)
      setPadding(0, 0, 0, 0)

      val itemTouchHelperCallback = ItemTouchHelperCallback(currentAdapter)
      val touchHelper = ItemTouchHelper(itemTouchHelperCallback)
      touchHelper.attachToRecyclerView(recyclerView)

      addItemDecoration(
        RecyclerItemDecorator(
          resources.getDimension(R.dimen.general_recycler_bottom_padding),
          horizontalDivider = resources.getDrawable(R.drawable.util_horizontal_divider, null)
        )
      )
    }

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(recyclerView))
  }

  private fun setupToolbar() {
    headerText.text = timetableName
    hideAndShowViews(viewsToGone = listOf(headerDetail))
    headerBackButton.setOnClickListener { onUpButtonClick() }
    headerRightSideText.setOnClickListener { presenter.revealProfile() }
  }

  override fun showNotInTimeMessage() {
    showSnackbarWithMessage(activity!! as AppCompatActivity, R.id.checklist_fragment_container,
        "Задача выполнена не вовремя")
  }
}