package com.gstudio.danil.myapplication.api

import com.gstudio.danil.myapplication.api.dto.*
import io.reactivex.Single
import okhttp3.MultipartBody
import retrofit2.Call
import retrofit2.http.*

interface Endpoints {

  // Логин и токен
  @POST("/api/v1/auth/login")
  fun postUser(@Body loginRequest: LoginRequest): Single<LoginDTO>

  // Hello Onboarding
  @GET("api/v1/onboarding/intro")
  fun getHelloOnboarding(): Single<HelloOnboardingDTO>

  // TODO обновлять токен по- человечески, когда будет реализация на Бэке
  // Запрос нового токена должен быть СИНХРОННЫМ
  @POST("/api/v1/auth/refresh")
  fun refreshToken(@Body refreshRequest: RefreshRequest): Call<LoginDTO>

  // Профиль
  @GET("/api/v1/users/profile")
  fun getProfile(): Single<ProfileDTO>

  // Инновации
  @GET("/api/v1/innovations/categories")
  fun getInnovationsCategories(): Single<InnovationsCategoriesDTO>

  @POST("/api/v1/innovations")
  fun postInnovation(@Body innovationsPost: InnovationsShareRequest): Single<InnovationsShareDTO>

  @GET("api/v1/onboarding/innovations")
  fun getInnovationsOnboarding(): Single<OnboardingDTO>

//  @Headers("Content-Type: multipart/form-data")
  @Multipart
  @POST("/api/v1/innovations/{id}/images")
  fun postInnovationPhoto(@Path(value = "id") innovationId: Int, @Part file: MultipartBody.Part): Single<InnovationsPhotoPostDTO>

  // Чек-листы
  @GET("/api/v1/checklists/roles")
  fun getChecklistsRoles(): Single<ChecklistRolesDTO>

  @GET("/api/v1/checklists/roles/{role_id}/categories")
  fun getChecklistTimetable(@Path(value = "role_id") roleId: Int): Single<ChecklistTimetableDTO>

  @GET("/api/v1/checklists/roles/{role_id}/categories/{category_id}")
  fun getChecklists(@Path(value = "role_id") roleId: Int, @Path(value = "category_id") categoryId: Int): Single<ChecklistsDTO>

  @PATCH("/api/v1/checklists/{item_id}")
  fun patchChecklist(@Path(value = "item_id") itemId: Int, @Body checklistPatch: ChecklistPatchRequest): Single<UserChecklistStatus>

  // Журнал смены
  @POST("/api/v1/shift-logs/today/shift-events")
  fun postWorkingDayEvent(@Body workingDayEventRequest: WorkingDayEventRequest): Single<WorkingDayEventDTO>

  @GET("/api/v1/shift-logs/today/shift-events")
  fun getWorkingDayEvents(): Single<WorkingDayEventsDTO>

  @GET("/api/v1/shift-logs/{shift_id}/shift-events")
  fun getShiftEvents(@Path(value = "shift_id") shiftId: Int): Single<WorkingDayEventsDTO>

  @GET("/api/v1/shift-logs")
  fun getWorkingDayPreviousLists(@Query("page") page:Int): Single<WorkingDayPreviousListsDTO>

  @GET("/api/v1/shift-logs/shift-events")
  fun getSearchResult(@Query("search") searchText: String?, @Query("page") page: Int): Single<WorkingDayEventsDTO>


  @POST("/api/v1/shift-logs/finish")
  fun finishWorkingDay(): Single<WorkingDayFinishDTO>

}
