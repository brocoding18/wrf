package com.gstudio.danil.myapplication.mvp

import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router
import java.lang.ref.WeakReference
import javax.inject.Inject

abstract class BasePresenter<View : BaseView>(private val router: Router) : Presenter<View> {

  private var weakView: WeakReference<View>? = null

  @Inject lateinit var disposable: CompositeDisposable

  override fun attachView(view: View) {
    if (!isViewAttached) {
      weakView = WeakReference(view)
      view.setPresenter(this)
    }
  }

  override fun detachView() {
    weakView?.clear()
    weakView = null
    disposable.clear()
    // TODO чтобы добавить этот кусок кода сюда, нужно перевести Login и Hello screen'ы на Cicerone
//    router.exit()
  }

  val view: View?
    get() = weakView?.get()

  private val isViewAttached: Boolean
    get() = weakView != null && weakView!!.get() != null

}