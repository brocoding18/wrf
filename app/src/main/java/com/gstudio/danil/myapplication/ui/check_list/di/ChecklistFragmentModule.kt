package com.gstudio.danil.myapplication.ui.check_list.di

import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistFragmentView
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistPresenter
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistPresenterImp
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesPresenter
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesPresenterImp
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesView
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetableFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetablePresenter
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetablePresenterImp
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetableView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router

@Module
class ChecklistFragmentModule {

  @Provides
  @PerFragment
  fun provideChecklistRolesFragmentView(
    checklistRolesFragment: ChecklistRolesFragment
  ): ChecklistRolesView = checklistRolesFragment

  @Provides
  @PerFragment
  fun provideChecklistTimetableFragmentView(
    checklistTimetableFragment: ChecklistTimetableFragment
  ): ChecklistTimetableView = checklistTimetableFragment

  @Provides
  @PerFragment
  fun provideChecklistFragmentFragmentView(
    checklistFragment: ChecklistFragment
  ): ChecklistFragmentView = checklistFragment

  @Provides
  @PerFragment
  fun provideChecklistRolesPresenter(
    rolesView: ChecklistRolesView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): ChecklistRolesPresenter =
    ChecklistRolesPresenterImp(rolesView, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideChecklistTimetablePresenter(
    timetableView: ChecklistTimetableView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): ChecklistTimetablePresenter =
    ChecklistTimetablePresenterImp(timetableView, apiService, router, spHelper)

  @Provides
  @PerFragment
  // TODO переименуй название метода, и сам презентер
  fun providePresenter(
    view: ChecklistFragmentView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): ChecklistPresenter =
    ChecklistPresenterImp(view, apiService, router, spHelper)

}