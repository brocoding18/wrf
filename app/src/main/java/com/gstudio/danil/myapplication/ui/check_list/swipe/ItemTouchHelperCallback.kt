package com.gstudio.danil.myapplication.ui.check_list.swipe

import android.graphics.Canvas
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.helper.ItemTouchHelper

class ItemTouchHelperCallback(private val adapter: ItemTouchHelperAdapter) : ItemTouchHelper.Callback() {
  override fun onMove(p0: RecyclerView, p1: ViewHolder, p2: ViewHolder): Boolean = false

  override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
    val swipeFlags = ItemTouchHelper.START or ItemTouchHelper.END
    return ItemTouchHelper.Callback.makeFlag(ItemTouchHelper.ACTION_STATE_SWIPE, swipeFlags)
  }

  override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
    adapter.onItemSwipe(viewHolder, viewHolder.adapterPosition, direction)
  }

  override fun getMoveThreshold(viewHolder: RecyclerView.ViewHolder): Float {
    return 0.4f
  }

  override fun onChildDraw(canvas: Canvas, recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder,
    dX: Float, dY: Float, actionState: Int, isCurrentlyActive: Boolean) {
    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
      val viewItem = viewHolder.itemView
      SwipeBackgroundHelper.paintDrawCommandToStart(
          canvas, viewItem, dX
      )
    }
    super.onChildDraw(canvas, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
  }

  interface ItemTouchHelperAdapter {

    fun onItemSwipe(holder: ViewHolder, position: Int, direction: Int)
  }
}