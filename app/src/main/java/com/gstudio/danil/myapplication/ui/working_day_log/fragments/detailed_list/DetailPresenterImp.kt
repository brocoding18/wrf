package com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list

import android.content.res.Resources
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventsDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayNotebookItem
import com.gstudio.danil.myapplication.utils.ext.formattedTime
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class DetailPresenterImp @Inject constructor(
  val resources: Resources,
  val detailedListView: DetailedListView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<DetailedListView>(router), DetailPresenter {

  fun getDetailList(shiftId: Int) {
    disposable.add(
      apiService.getShiftEvents(shiftId)
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: WorkingDayEventsDTO ->
            val result = ArrayList<WorkingDayItem>()

            response.workingDayEvents.forEach { it ->
              result.add(WorkingDayNotebookItem(
                id = it.id,
                text = it.eventText,
                timeAndPerson = resources.getString(R.string.working_day_item_time_person, formattedTime(it.createdAt), it.user.name)
              ))
            }

            detailedListView.setupAdapter(result)
          },
            { e: Throwable ->
              val errorBody = (e as? HttpException)?.response()
                  ?.errorBody()
              println(errorBody)
            })
    )
  }
}