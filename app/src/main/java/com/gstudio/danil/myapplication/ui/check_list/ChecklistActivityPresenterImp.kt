package com.gstudio.danil.myapplication.ui.check_list

import com.gstudio.danil.myapplication.Screens.ChecklistRolesScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ChecklistActivityPresenterImp @Inject constructor(
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
): BasePresenter<ChecklistActivityView>(router), ChecklistActivityPresenter {

  override fun revealRolesScreen() {
    router.newRootScreen(ChecklistRolesScreen)
  }
}