package com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding

import com.gstudio.danil.myapplication.Screens.InnovationCategoriesScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MessagePresenterImp @Inject constructor(
  val messageView: MessageView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<MessageView>(router), MessagePresenter {

  fun onLayoutClick() {
    // TODO надо бы это как-то убрать отсуда
    if (!MessageFragment.isLastPage) {
      router.newRootScreen(InnovationCategoriesScreen)
    }

  }
}