package com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook

import android.content.res.Resources
import com.gstudio.danil.myapplication.Screens
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventsDTO
import com.gstudio.danil.myapplication.api.dto.WorkingDayFinishDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayLab
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class NotebookPresenterImp @Inject constructor(
  val resources: Resources,
  val notebookView: NotebookView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<NotebookView>(router), NotebookPresenter {

  fun getEvents() {
    disposable.add(
      apiService.getWorkingDayEvents()
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: WorkingDayEventsDTO ->
            WorkingDayLab.workingDayEvents = response.workingDayEvents

            notebookView.setupNotebookAdapter()
          },
            { e: Throwable ->
              val errorBody = (e as? HttpException)?.response()
                  ?.errorBody()
              println(errorBody)
            })
    )
  }

  fun onWorkingDayFinish() {
    disposable.add(
        apiService.finishWorkingDay()
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ response: WorkingDayFinishDTO ->
              println(response)
            },
                { e: Throwable ->
                  val errorBody = (e as? HttpException)?.response()
                      ?.errorBody()
                  println(errorBody)
                })
    )
  }

  fun goToDetail(shiftId: Int, person: String?, closeDate: String?) {
    router.navigateTo(Screens.WorkingDayDetailedList(shiftId, person, closeDate))
  }

  fun addEventScreen() {
    router.navigateTo(Screens.WorkingDayAddEvent)
  }
}