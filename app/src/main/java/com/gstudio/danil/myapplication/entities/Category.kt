package com.gstudio.danil.myapplication.entities

class Category(
  var id: Int,
  var name: String,
  val layoutType: LayoutType = LayoutType()
)

// Default - Подходит для списков категорий Инновации и все списков Чеклистов
// Menu - для списка экрана Меню

data class LayoutType (
  val type: String = "Default"
)

