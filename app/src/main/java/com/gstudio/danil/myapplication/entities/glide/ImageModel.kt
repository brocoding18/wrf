package com.gstudio.danil.myapplication.entities.glide

data class ImageModel(
  val imageUrl: String
)