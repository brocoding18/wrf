package com.gstudio.danil.myapplication.ui.main_menu

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.Constants.MainMenuData.CHECKLIST
import com.gstudio.danil.myapplication.Constants.MainMenuData.INNOVATIONS
import com.gstudio.danil.myapplication.Constants.MainMenuData.WORKING_DAY_LOG
import com.gstudio.danil.myapplication.Constants.MainMenuData.menuItems
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.mvp.BaseActivity
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter.OnItemClickListener
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class MenuActivity : BaseActivity(), MenuView {
  companion object {
    fun newIntent(packageContext: Context): Intent =
      Intent(packageContext, MenuActivity::class.java)
  }

  private val options: ArrayList<Category> = menuItems

  @BindView(R.id.menu_recycler) lateinit var recyclerView: RecyclerView

  @Inject lateinit var presenter: MenuPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.main_menu_layout)
    ButterKnife.bind(this)

    val currentAdapter = SingleTextViewAdapter(options, object : OnItemClickListener {
      override fun onItemClick(optionsItem: Category) = formOnItemClick(optionsItem)
    })

    recyclerView.apply {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(this@MenuActivity)
      isLayoutFrozen = true
    }
  }

  override fun onResume() {
    super.onResume()
  }

  override fun formOnItemClick(optionsItem: Category) {
    when (optionsItem) {
      CHECKLIST -> presenter.onChecklist()
      WORKING_DAY_LOG -> presenter.onWorkingDay()
      //          TEAM ->
      //          MISTAKES ->
      INNOVATIONS -> presenter.onInnovations()
      //          STOP_LISTS ->
      else -> Toast.makeText(this@MenuActivity, optionsItem.name, Toast.LENGTH_SHORT).show()
    }
  }

  @OnClick(R.id.menu_avatar)
  fun onMenuAvatarClick() {
    presenter.onMenuAvatarClick()
  }
}