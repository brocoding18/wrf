package com.gstudio.danil.myapplication.ui.main_menu

import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.mvp.BaseView

interface MenuView : BaseView {
  fun formOnItemClick(optionsItem: Category)
}