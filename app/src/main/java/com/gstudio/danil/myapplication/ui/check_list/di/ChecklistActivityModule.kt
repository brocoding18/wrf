package com.gstudio.danil.myapplication.ui.check_list.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivity
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivityPresenter
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivityPresenterImp
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivityView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class ChecklistActivityModule {

  @Provides
  @PerActivity
  fun provideChecklistView(checklistActivity: ChecklistActivity): ChecklistActivityView =
    checklistActivity

  @Provides
  @PerActivity
  fun provideChecklistNavigator(activity: ChecklistActivity): SupportAppNavigator =
    SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun provideChecklistActivityPresenter(
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): ChecklistActivityPresenter = ChecklistActivityPresenterImp(apiService, router, spHelper)

}
