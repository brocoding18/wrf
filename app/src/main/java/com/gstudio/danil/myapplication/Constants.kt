package com.gstudio.danil.myapplication

import com.gstudio.danil.myapplication.api.dto.Checklist
import com.gstudio.danil.myapplication.api.dto.ChecklistRoles
import com.gstudio.danil.myapplication.api.dto.ChecklistTimetable
import com.gstudio.danil.myapplication.api.dto.InnovationsCategories
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.entities.LayoutType

object Constants {

  val GENERAL_TEXT_SIZE = 20F
  val DATABASE_NAME = "WrfRoomDb.db"

  // Меню
  object MainMenuData {
    val CHECKLIST = Category(1, "Чеклисты", LayoutType("Menu"))
    val WORKING_DAY_LOG = Category(2, "Журнал смены", LayoutType("Menu"))
//    val TEAM = Category(3, "Команда", LayoutType("Menu"))
//    val MISTAKES = Category(4, "Ошибки", LayoutType("Menu"))
    val INNOVATIONS = Category(5, "Иннованции", LayoutType("Menu"))
//    val STOP_LISTS = Category(6, "Стоп-листы", LayoutType("Menu"))

    val menuItems = arrayListOf(CHECKLIST, WORKING_DAY_LOG, /*TEAM, MISTAKES,*/ INNOVATIONS/*, STOP_LISTS*/)
  }

  // Чеклисты
  object ChecklistTimetableData {
    lateinit var checklistTimetable: ArrayList<ChecklistTimetable>
  }

  object ChecklistRolesData {
    lateinit var checklistRoles: ArrayList<ChecklistRoles>
  }

  object ChecklistsData {
    lateinit var checklists: ArrayList<Checklist>
  }

  object InnovationsThemesData {
    lateinit var innovationsThemes: ArrayList<InnovationsCategories>
  }

  // Журнал смены
  object WorkingDay {
    var TOTAL_PAGES = 0
    var SEARCH_TOTAL_PAGES = 0
    var SEARCH_STRING: String? = ""
  }

  object Integers {
    const val PAGE_SIZE = 30
    const val PREFETCH_SIZE = 30
  }

  object Strings {
    // General
    const val PHONE_MASK = "+7 ([000]) [000] [00] [00]"
    const val ATTACH_PHOTO = "Прикрепить фото"
    const val DETACH_PHOTO = "Открепить фото"
    const val AUTHORIZATION_HEADER = "Authorization"

    // Checklist status
    const val NOT_DONE = "NotDone"
    const val DONE = "Done"
    const val OVERDUE = "Overdue"
    // Innovations Categories
    const val INNOVATIONS_CATEGORIES_HEADER = "Выберите тему"
    const val INNOVATIONS_CATEGORIES_HEADER_DETAIL = "Выберете тему в которой вы хотите предложить инновациию"
    // Innovations Description
    const val INNOVATIONS_DESCRIPTION_HEADER = "Описание идеи"
    const val INNOVATIONS_DESCRIPTION_HEADER_DETAIL = "Опишите максимально детально, что вы хотите предложить"
    const val INNOVATIONS_DESCRIPTION_SEND_BUTTON_TEXT = "Отправить идею"
  }
}