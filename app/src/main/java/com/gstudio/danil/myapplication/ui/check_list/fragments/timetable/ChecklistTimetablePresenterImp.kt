package com.gstudio.danil.myapplication.ui.check_list.fragments.timetable

import com.gstudio.danil.myapplication.Constants.ChecklistTimetableData
import com.gstudio.danil.myapplication.Screens
import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.ChecklistTimetableDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ChecklistTimetablePresenterImp @Inject constructor(
  val timetableView: ChecklistTimetableView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<ChecklistTimetableView>(router), ChecklistTimetablePresenter {

  fun getTimetable(roleId: Int) {
    disposable.add(
      apiService.getChecklistTimetable(roleId)
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: ChecklistTimetableDTO ->
            ChecklistTimetableData.checklistTimetable = response.checklistTimetable

            timetableView.setupAdapter()
          },
            { e: Throwable ->
              val errorBody = (e as? HttpException)?.response()
                  ?.errorBody()
              println(errorBody)
            })
    )
  }

  fun revealChecklistFragmentScreen(roleId: Int, timetableId: Int, timetableName: String) {
    router.navigateTo(Screens.ChecklistScreen(roleId, timetableId, timetableName))
  }

  fun revealProfile() {
    router.navigateTo(ProfileActivityScreen)
  }

}