package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class ChecklistRolesDTO(
  @SerializedName("data") val checklistRoles: ArrayList<ChecklistRoles>
)

data class ChecklistRoles(
  @SerializedName("id") val id: Int,
  @SerializedName("name") val name: String
)