package com.gstudio.danil.myapplication.ui.splash

import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class SplashPresenterImp @Inject constructor(
  private val splashView: SplashView,
  private val router: Router,
  private val api: Endpoints
) : BasePresenter<SplashView>(router), SplashPresenter {


}