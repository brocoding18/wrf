package com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list

import com.gstudio.danil.myapplication.mvp.BaseView
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem

interface DetailedListView : BaseView {
  fun setupAdapter(list: ArrayList<WorkingDayItem>)
}