package com.gstudio.danil.myapplication.ui.main_menu

interface MenuPresenter {
  fun onBackCommandClick()
  fun onInnovations()
  fun onChecklist()
  fun onWorkingDay()
}