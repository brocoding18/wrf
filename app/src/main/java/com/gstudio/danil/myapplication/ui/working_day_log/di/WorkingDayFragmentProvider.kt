package com.gstudio.danil.myapplication.ui.working_day_log.di

import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailedListFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class WorkingDayFragmentProvider{

  @PerFragment
  @ContributesAndroidInjector(modules = [WorkingDayFragmentModule::class])
  abstract fun provideWorkingDayNotebookFragmentFactory(): NotebookFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [WorkingDayFragmentModule::class])
  abstract fun provideWorkingDayDetailedListFragmentFactory(): DetailedListFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [WorkingDayFragmentModule::class])
  abstract fun provideWorkingDayAddFragmentFactory(): AddFragment

}