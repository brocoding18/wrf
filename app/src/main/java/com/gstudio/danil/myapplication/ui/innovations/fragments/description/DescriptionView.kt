package com.gstudio.danil.myapplication.ui.innovations.fragments.description

import com.gstudio.danil.myapplication.mvp.BaseView

interface DescriptionView : BaseView {
  fun onShareButtonClick()
  fun onAttachPhotoClick()
}