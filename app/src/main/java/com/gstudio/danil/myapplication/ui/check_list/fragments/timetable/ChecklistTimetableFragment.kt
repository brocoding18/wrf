package com.gstudio.danil.myapplication.ui.check_list.fragments.timetable

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.Constants.ChecklistTimetableData.checklistTimetable
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter.OnItemClickListener
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import javax.inject.Inject

class ChecklistTimetableFragment : BaseFragment(), ChecklistTimetableView {
  companion object {
    private const val ARG_CHECKLIST_ROLE_ID = "checklist_role_id"
    private const val ARG_CHECKLIST_ROLE_NAME = "checklist_role_name"

    fun newInstance(roleId: Int, roleName: String): ChecklistTimetableFragment {
      val args = Bundle()
      args.putInt(ARG_CHECKLIST_ROLE_ID, roleId)
      args.putString(ARG_CHECKLIST_ROLE_NAME, roleName)

      val fragment = ChecklistTimetableFragment()
      fragment.arguments = args
      return fragment
    }
  }

  private lateinit var options: List<Category>

  @BindView(R.id.util_general_header) lateinit var headerText: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar_right_side_text) lateinit var headerRightSideText: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout

  @BindView(R.id.util_fragment_recycler) lateinit var recyclerView: RecyclerView

  @BindView(R.id.util_fragment_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: ChecklistTimetablePresenterImp

  private var roleId: Int? = null
  private var roleName: String? = null

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_header_and_recycler_view)
    ButterKnife.bind(this, view)

    unboxArguments()
    setupHeader()

    return view
  }

  override fun onResume() {
    super.onResume()
    presenter.getTimetable(roleId!!)
  }

  private fun unboxArguments() {
    roleId = arguments!!.getInt(ARG_CHECKLIST_ROLE_ID)
    roleName = arguments!!.getString(ARG_CHECKLIST_ROLE_NAME)
  }

  override fun setupAdapter() {
    options = checklistTimetable.map { it ->
      Category(it.id, it.name)
    }

    val currentAdapter = SingleTextViewAdapter(options, object : OnItemClickListener {
      override fun onItemClick(timetableItem: Category) {
        presenter.revealChecklistFragmentScreen(roleId!!, timetableItem.id, timetableItem.name)
      }
    })

    recyclerView.apply {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(activity!!)
      isLayoutFrozen = true
    }

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(recyclerView))
  }

  private fun setupHeader() {
    headerText.text = roleName
    hideAndShowViews(viewsToHide = listOf(headerDetail))

    headerBackButton.setOnClickListener { onUpButtonClick() }
    headerRightSideText.setOnClickListener { presenter.revealProfile() }
  }
}