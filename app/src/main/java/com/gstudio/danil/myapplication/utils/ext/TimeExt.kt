package com.gstudio.danil.myapplication.utils.ext

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat

// TODO сделаем фабрику?
fun formattedTime(timeString: String?): String? {
  if (timeString == null) return null

  val formatter = DateTimeFormat.forPattern("HH:mm")
  val time: DateTime = ISODateTimeFormat.dateTimeParser()
      .parseDateTime(timeString)
  return formatter.print(time)
}

fun formattedDate(timeString: String?): String? {
  if (timeString == null) return null

  val formatter = DateTimeFormat.forPattern("dd.MM.yy")
  val time: DateTime = ISODateTimeFormat.dateTimeParser()
      .parseDateTime(timeString)
  return formatter.print(time)
}