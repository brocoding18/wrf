package com.gstudio.danil.myapplication

import android.content.Context
import android.content.Intent
import android.support.v4.app.Fragment
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivity
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.roles.ChecklistRolesFragment
import com.gstudio.danil.myapplication.ui.check_list.fragments.timetable.ChecklistTimetableFragment
import com.gstudio.danil.myapplication.ui.innovations.InnovationsActivity
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import com.gstudio.danil.myapplication.ui.profile.ProfileActivity
import com.gstudio.danil.myapplication.ui.profile.fragment.ProfileFragment
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayActivity
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailedListFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

object Screens {
  // Profile
  object ProfileActivityScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent = Intent(context, ProfileActivity::class.java)
  }

  object ProfileFragmentScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = ProfileFragment.newInstance()
  }

  // Checklists
  object ChecklistActivityScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent = Intent(context, ChecklistActivity::class.java)
  }

  object ChecklistRolesScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = ChecklistRolesFragment.newInstance()
  }

  data class ChecklistTimetableScreen(val roleId: Int, val roleName: String) : SupportAppScreen() {
    override fun getFragment(): Fragment = ChecklistTimetableFragment.newInstance(roleId, roleName)
  }

  // TODO перекрываются имена - ChecklistActivityScreen и ChecklistScreen
  data class ChecklistScreen(val roleId: Int, val timetableId: Int, val timetableName: String) : SupportAppScreen() {
    override fun getFragment(): Fragment = ChecklistFragment.newInstance(roleId, timetableId, timetableName)
  }

  // WorkingDay
  object WorkingDayScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent = Intent(context, WorkingDayActivity::class.java)
  }

  object WorkingDayNotebook : SupportAppScreen() {
    override fun getFragment(): Fragment = NotebookFragment.newInstance()
  }

  object WorkingDayAddEvent : SupportAppScreen() {
    override fun getFragment(): Fragment = AddFragment.newInstance()
  }

  data class WorkingDayDetailedList(val shiftId: Int, val person: String?, val closeDate: String?): SupportAppScreen() {
    override fun getFragment(): Fragment = DetailedListFragment.newInstance(shiftId, person, closeDate)
  }

  // Innovations
  object InnovationActivityScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent = Intent(context, InnovationsActivity::class.java)
  }

  // Data class'ы используем для проброса данных во фрагмент
  data class InnovationsMessageScreen(var message: String?) : SupportAppScreen() {
    override fun getFragment() : Fragment = MessageFragment.newInstance(message)
  }

  data class InnovationDescriptionScreen(var id: Int) : SupportAppScreen() {
    override fun getFragment() : Fragment = DescriptionFragment.newInstance(id)
  }

  object InnovationCategoriesScreen : SupportAppScreen() {
    override fun getFragment() : Fragment = CategoriesFragment.newInstance()
  }
}
