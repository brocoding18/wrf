package com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.WorkingDayAdapter.OnStoryItemClickListener
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayNotebookItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayStoryItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayTemperatureItem

abstract class WorkingDayHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
  abstract fun bindItem(item: WorkingDayItem)
}

class WorkingDayTemperatureHolder(itemView: View) : WorkingDayHolder(itemView) {
  private lateinit var temperatureItem: WorkingDayTemperatureItem

  init { ButterKnife.bind(this, itemView) }

  override fun bindItem(item: WorkingDayItem) {
    temperatureItem = item as WorkingDayTemperatureItem
  }
}

class WorkingDayStoryHolder(itemView: View, listenerStory: OnStoryItemClickListener?) : WorkingDayHolder(itemView) {

  private lateinit var storyItem: WorkingDayStoryItem

  @BindView(R.id.working_day_story_item_date) lateinit var storyDate: TextView
  @BindView(R.id.working_day_story_item_person_name) lateinit var storyPerson: TextView
  @BindView(R.id.working_day_story_item_time) lateinit var storyTime: TextView

  init {
    itemView.setOnClickListener {
      if (adapterPosition != RecyclerView.NO_POSITION) {
        listenerStory?.onItemClick(storyItem)
      }
    }

    ButterKnife.bind(this, itemView)
  }

  override fun bindItem(item: WorkingDayItem) {
    storyItem = item as WorkingDayStoryItem

    with(item) {
      storyDate.text = date
      storyPerson.text = person
      storyTime.text = itemView.context.resources.getString(R.string.working_day_story_time, time)
    }
  }
}

class WorkingDayNotebookHolder(itemView: View) : WorkingDayHolder(itemView) {

  private lateinit var workingDayNotebookItem: WorkingDayNotebookItem

  @BindView(R.id.working_day_notebook_item_time_person) lateinit var notebookTimePerson: TextView
  @BindView(R.id.working_day_notebook_item_text) lateinit var notebookItemText: TextView

  init { ButterKnife.bind(this, itemView) }

  override fun bindItem(item: WorkingDayItem) {
    workingDayNotebookItem = item as WorkingDayNotebookItem

    with(item) {
      notebookTimePerson.text = timeAndPerson
      notebookItemText.text = text
    }
  }
}