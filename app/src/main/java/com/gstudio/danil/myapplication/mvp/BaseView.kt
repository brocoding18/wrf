package com.gstudio.danil.myapplication.mvp

interface BaseView {
//  fun onError()
  fun setPresenter(presenter: BasePresenter<*>)
}