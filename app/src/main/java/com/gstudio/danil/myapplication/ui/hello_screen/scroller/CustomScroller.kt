package com.gstudio.danil.myapplication.ui.hello_screen.scroller

import android.content.Context
import android.view.animation.Interpolator
import android.widget.Scroller

class CustomScroller(context: Context, interpolator: Interpolator, duration: Int)
  : Scroller(context, interpolator) {

  private var mDuration = duration

  override fun startScroll(
    startX: Int,
    startY: Int,
    dx: Int,
    dy: Int,
    duration: Int
  ) {
    // Ignore received duration, use fixed one instead
    super.startScroll(startX, startY, dx, dy, mDuration)
  }
}