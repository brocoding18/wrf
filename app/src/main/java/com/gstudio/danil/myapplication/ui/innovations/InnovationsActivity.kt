package com.gstudio.danil.myapplication.ui.innovations

import android.os.Bundle
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class InnovationsActivity : BaseActivity(), InnovationsView {

  @Inject lateinit var presenter: InnovationsPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.utils_activity_fragment)

    // TODO выглядит стремно. Это нужно, чтобы использовать только один класс MessageFragment?
    MessageFragment.isLastPage = false

    presenter.firstScreen()
  }
}