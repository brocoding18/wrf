package com.gstudio.danil.myapplication.ui.check_list.adapter

import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.ViewHolder
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.Constants.Strings.DONE
import com.gstudio.danil.myapplication.Constants.Strings.NOT_DONE
import com.gstudio.danil.myapplication.Constants.Strings.OVERDUE
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.dto.Checklist
import com.gstudio.danil.myapplication.api.dto.ChecklistPatchRequest
import com.gstudio.danil.myapplication.ui.check_list.adapter.ChecklistAdapter.CheckListHolder
import com.gstudio.danil.myapplication.ui.check_list.fragments.checklists.ChecklistPresenterImp
import com.gstudio.danil.myapplication.ui.check_list.swipe.ItemTouchHelperCallback.ItemTouchHelperAdapter
import com.gstudio.danil.myapplication.utils.ext.formattedTime
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.setBackgroundColorExt
import org.joda.time.DateTime
import org.joda.time.DateTimeComparator
import org.joda.time.format.ISODateTimeFormat
import java.util.*

class ChecklistAdapter(
  private val checklistModel: MutableList<Checklist>,
  val presenter: ChecklistPresenterImp
) : RecyclerView.Adapter<CheckListHolder>(), ItemTouchHelperAdapter {

  private lateinit var currentHolder: ChecklistAdapter.CheckListHolder

  override fun onItemSwipe(holder: ViewHolder, position: Int, direction: Int) {
    currentHolder = holder as CheckListHolder
    currentHolder.changeStatus(position, direction)
  }

  override fun onCreateViewHolder(container: ViewGroup, viewType: Int): CheckListHolder =
    CheckListHolder(container.inflate(R.layout.checklist_item))

  override fun getItemCount(): Int = checklistModel.size

  override fun onBindViewHolder(holder: CheckListHolder, position: Int) =
    holder.bindItem(checklistModel[position])

  // TODO нужна ли холдеру ссылка на адаптер?
  inner class CheckListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val timeComparator = DateTimeComparator.getTimeOnlyInstance()!!
    private lateinit var checkListItem: Checklist

    @BindView(R.id.checklist_category) lateinit var category: TextView
    @BindView(R.id.checklist_time) lateinit var time: TextView
    @BindView(R.id.checklist_title) lateinit var title: TextView
    @BindView(R.id.checklist_description) lateinit var description: TextView
    @BindView(R.id.checklist_status_color) lateinit var status: View

    init { ButterKnife.bind(this, itemView) }

    fun bindItem(checklistModelItem: Checklist) {
      checkListItem = checklistModelItem

      // TODO переименуй в верстке
      category.text = checklistModelItem.title

      time.text = formattedTime(checklistModelItem.doneAt)
      title.text = checklistModelItem.subtitle
      description.text = checklistModelItem.description

      when {
        checklistModelItem.userCheckList == null -> status.setBackgroundColorExt(R.color.black)
        checklistModelItem.userCheckList!!.status == NOT_DONE -> status.setBackgroundColorExt(R.color.crimson)
        checklistModelItem.userCheckList!!.status == DONE -> status.setBackgroundColorExt(R.color.green)
        checklistModelItem.userCheckList!!.status == OVERDUE -> status.setBackgroundColorExt(R.color.yellow)
      }
    }

    fun changeStatus(position: Int, direction: Int) {
      when (direction) {
        ItemTouchHelper.START -> patchChecklist(NOT_DONE, position)
        ItemTouchHelper.END -> {
          if (notInTime()) {
            patchChecklist(OVERDUE, position)
          } else {
            patchChecklist(DONE, position)
          }
        }
      }
    }

    private fun notInTime(): Boolean {
      val currentTime = Calendar.getInstance().time
      // Форматируем дату с сервера для сравнения с текущей
      val time: DateTime = ISODateTimeFormat.dateTimeParser()
          .parseDateTime(checkListItem.doneAt)

      return timeComparator.compare(time, currentTime) < 0
    }

    private fun patchChecklist(status: String, position: Int) {
      presenter.patchChecklist(
        checkListItem.id, this, ChecklistPatchRequest(status), position
      )
    }

    fun changeStatusColorAndNotify(statusDescription: String, position: Int) {
      when (statusDescription) {
        NOT_DONE -> status.setBackgroundColorExt(R.color.crimson)
        DONE -> status.setBackgroundColorExt(R.color.green)
        OVERDUE -> {
          status.setBackgroundColorExt(R.color.yellow)
          presenter.showNotInTimeMessage()
        }
        else -> Log.e(CheckListHolder::class.java.simpleName, "Щито ща статус???")
      }

      notifyItemChanged(position)
    }
  }
}