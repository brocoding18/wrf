package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class ChecklistTimetableDTO(
  @SerializedName("data") val checklistTimetable: ArrayList<ChecklistTimetable>
)

data class ChecklistTimetable(
  @SerializedName("id") val id: Int,
  @SerializedName("name") val name: String
)