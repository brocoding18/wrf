package com.gstudio.danil.myapplication.ui.profile

import com.gstudio.danil.myapplication.Screens.ProfileFragmentScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class ProfileActivityPresenterImp @Inject constructor(
  private val api: Endpoints,
  private val router: Router
) : BasePresenter<ProfileView>(router), ProfileActivityPresenter {

  fun revealProfileScreen() {
    router.newRootScreen(ProfileFragmentScreen)
  }
}