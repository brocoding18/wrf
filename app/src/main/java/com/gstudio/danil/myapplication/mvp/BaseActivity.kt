package com.gstudio.danil.myapplication.mvp

import dagger.android.support.DaggerAppCompatActivity
import ru.terrakok.cicerone.NavigatorHolder
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

abstract class BaseActivity: DaggerAppCompatActivity(), BaseView {

  @Inject lateinit var navigatorHolder: NavigatorHolder

  private var presenter: BasePresenter<*>? = null

  abstract var navigator: SupportAppNavigator

  override fun setPresenter(presenter: BasePresenter<*>) {
    this.presenter = presenter
  }

  fun onError() {
//    toast("Something went wrong")
  }

  override fun onStart() {
    super.onStart()
//    EventBus.getDefault().register(this)
  }

  override fun onResume() {
    super.onResume()
    navigatorHolder.setNavigator(navigator)
  }

  override fun onPause() {
    super.onPause()
    navigatorHolder.removeNavigator()
  }

  override fun onStop() {
    //    EventBus.getDefault().unregister(this)

    super.onStop()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter?.detachView()
    presenter = null
  }

}