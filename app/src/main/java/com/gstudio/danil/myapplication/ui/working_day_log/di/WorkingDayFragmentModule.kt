package com.gstudio.danil.myapplication.ui.working_day_log.di

import android.arch.paging.PagedList
import android.arch.paging.PagedList.Config
import android.content.res.Resources
import com.gstudio.danil.myapplication.Constants.Integers.PAGE_SIZE
import com.gstudio.danil.myapplication.Constants.Integers.PREFETCH_SIZE
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.notebook.WorkingDayDataSource
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.notebook.WorkingDayDataSourceFactory
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list.SearchDataSource
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list.SearchDataSourceFactory
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddEventPresenter
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddEventPresenterImp
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event.AddView
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailPresenter
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailPresenterImp
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailedListFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list.DetailedListView
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookFragment
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookPresenter
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookPresenterImp
import com.gstudio.danil.myapplication.ui.working_day_log.fragments.notebook.NotebookView
import dagger.Module
import dagger.Provides
import io.reactivex.disposables.CompositeDisposable
import ru.terrakok.cicerone.Router

@Module
class WorkingDayFragmentModule {

  @Provides
  @PerFragment
  fun provideNotebookFragment(
    workingDayNotebookFragment: NotebookFragment
  ): NotebookView = workingDayNotebookFragment

  @Provides
  @PerFragment
  fun provideAddFragment(
    workingDayAddFragment: AddFragment
  ): AddView = workingDayAddFragment

  @Provides
  @PerFragment
  fun provideDetailedListFragment(
    workingDayDetailedListFragment: DetailedListFragment
  ): DetailedListView = workingDayDetailedListFragment

  @Provides
  @PerFragment
  fun provideDetailPresenter(
    resources: Resources,
    detailedListView: DetailedListView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): DetailPresenter =
    DetailPresenterImp(resources, detailedListView, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideNotebookPresenter(
    resources: Resources,
    notebookView: NotebookView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): NotebookPresenter =
    NotebookPresenterImp(resources, notebookView, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideAddEventPresenter(
    addView: AddView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): AddEventPresenter =
    AddEventPresenterImp(addView, apiService, router, spHelper)

  @Provides
  @PerFragment
  fun provideDataSourceFactory(workingDayDataSource: WorkingDayDataSource): WorkingDayDataSourceFactory =
      WorkingDayDataSourceFactory(workingDayDataSource)

  @Provides
  @PerFragment
  fun provideDataSource(workingDayView: NotebookView, resources: Resources, apiService: Endpoints, disposable: CompositeDisposable): WorkingDayDataSource =
      WorkingDayDataSource(workingDayView, resources, apiService, disposable)

  @Provides
  @PerFragment
  fun provideSearchDataSource(resources: Resources, apiService: Endpoints, disposable: CompositeDisposable) =
    SearchDataSource(resources, apiService, disposable)

  @Provides
  @PerFragment
  fun provideSearchDataSourceFactory(dataSource: SearchDataSource) =
    SearchDataSourceFactory(dataSource)

  @Provides
  @PerFragment
  fun provideDataSourceConfig(): Config = PagedList.Config.Builder()
      .setEnablePlaceholders(false)
      .setInitialLoadSizeHint(10)
      .setPageSize(PAGE_SIZE)
      .setPrefetchDistance(PREFETCH_SIZE)
      .build()

}