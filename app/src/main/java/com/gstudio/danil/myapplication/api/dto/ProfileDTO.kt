package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class ProfileDTO(
  @SerializedName("id") val id: Int,
  @SerializedName("name") val name: String?,
  @SerializedName("image_url") val imageUrl: String?,
  @SerializedName("position") val position: String?,
  @SerializedName("restaurant") val restaurant: Restaurant?,
  @SerializedName("check_lists_done") val checkListsDone: Int,
  @SerializedName("check_lists_total") val checkListsTotal: Int
)

data class Restaurant(
  @SerializedName("id") val id: Int,
  @SerializedName("name") val name: String?
)