package com.gstudio.danil.myapplication.ui.login.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.sp.SpHelper
import com.gstudio.danil.myapplication.ui.login.LoginActivity
import com.gstudio.danil.myapplication.ui.login.LoginPresenter
import com.gstudio.danil.myapplication.ui.login.LoginPresenterImp
import com.gstudio.danil.myapplication.ui.login.LoginView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class LoginActivityModule {

  @Provides
  @PerActivity
  fun provideLoginView(loginActivity: LoginActivity): LoginView = loginActivity

  @Provides
  @PerActivity
  fun provideLoginNavigator(activity: LoginActivity): SupportAppNavigator = SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun provideLoginPresenter(
    loginView: LoginView,
    apiService: Endpoints,
    router: Router,
    spHelper: SpHelper
  ): LoginPresenter =
    LoginPresenterImp(loginView, apiService, router, spHelper)

}
