package com.gstudio.danil.myapplication.ui.innovations.di

import com.gstudio.danil.myapplication.di.PerFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.categories.CategoriesFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.description.DescriptionFragment
import com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding.MessageFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class InnovationsFragmentProvider {

  @PerFragment
  @ContributesAndroidInjector(modules = [InnovationsFragmentModule::class])
  abstract fun provideInnovationsHelloByeFragmentFactory(): MessageFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [InnovationsFragmentModule::class])
  abstract fun provideInnovationsCategoriesFragmentFactory(): CategoriesFragment

  @PerFragment
  @ContributesAndroidInjector(modules = [InnovationsFragmentModule::class])
  abstract fun provideInnovationsDescriptionFragmentFactory(): DescriptionFragment


}