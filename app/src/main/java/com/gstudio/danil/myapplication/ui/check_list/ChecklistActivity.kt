package com.gstudio.danil.myapplication.ui.check_list

import android.os.Bundle
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class ChecklistActivity : BaseActivity(), ChecklistActivityView {

  @Inject lateinit var presenter: ChecklistActivityPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.utils_activity_fragment)

    presenter.revealRolesScreen()
  }
}