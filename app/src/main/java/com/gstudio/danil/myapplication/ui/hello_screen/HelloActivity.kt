package com.gstudio.danil.myapplication.ui.hello_screen

import android.content.Context
import android.content.Intent
import android.graphics.Point
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.animation.DecelerateInterpolator
import android.view.animation.Interpolator
import android.widget.HorizontalScrollView
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnPageChange
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.glide.GlideApp
import com.gstudio.danil.myapplication.ui.hello_screen.scroller.CustomPageTransformer
import com.gstudio.danil.myapplication.ui.hello_screen.scroller.CustomScroller

private const val CUSTOM_SCROLL_DURATION = 350

class HelloActivity : AppCompatActivity() {
  companion object {
    fun newIntent(packageContext: Context): Intent {
      return Intent(packageContext, HelloActivity::class.java)
    }
  }

  private var isProfileReady = false

  @BindView(R.id.hello_view_pager) lateinit var viewPager: ViewPager
  @BindView(R.id.hello_background_img) lateinit var backgroundImage: ImageView
  @BindView(R.id.hello_scroll_view) lateinit var helloScroll: HorizontalScrollView
  @BindView(R.id.hello_tab_dots) lateinit var tab: TabLayout

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.hello_screen_layout)
    ButterKnife.bind(this)

    setCustomScrollerToViewPager()

    val display = windowManager.defaultDisplay
    val size = Point()
    display.getSize(size)
    val width = size.x
    val height = size.y

    GlideApp.with(this)
        .load("http://summergirls.ru:8082/api/v1/resize/${height}x$width/onboarding/image")
        .into(backgroundImage)

    viewPager.setPageTransformer(false, CustomPageTransformer())

    setupAdapter()
  }

  @OnPageChange(
      value = [R.id.hello_view_pager],
      callback = OnPageChange.Callback.PAGE_SCROLLED)
  fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
    val x =
    ((viewPager.width * position + positionOffsetPixels) * computeFactor()).toInt()
    helloScroll.scrollTo(x, 0)
  }

  @OnPageChange(
      value = [R.id.hello_view_pager],
      callback = OnPageChange.Callback.PAGE_SCROLL_STATE_CHANGED)
  fun onPageScrollStateChanged(state: Int) {
    isProfileReady = state == ViewPager.SCROLL_STATE_IDLE && viewPager.currentItem == 2

    if (isProfileReady) {
      HelloFragment.isLastPage = true
    }
  }

  private fun computeFactor(): Float =
    (backgroundImage.width - viewPager.width) /
        (viewPager.width * (viewPager.adapter!!.count - 1)).toFloat()


  private fun setupAdapter() {
    viewPager.adapter = HelloPagerAdapter(supportFragmentManager)

    tab.setupWithViewPager(viewPager, true)
  }

  private fun setCustomScrollerToViewPager(
    interpolator: Interpolator = DecelerateInterpolator(),
    duration: Int = CUSTOM_SCROLL_DURATION
  ) {
    try {
      val customScroller = CustomScroller(this, interpolator, duration)
      val mScroller = ViewPager::class.java.getDeclaredField("mScroller")
      mScroller.isAccessible = true
      mScroller.set(viewPager, customScroller)
    } catch (e: NoSuchFieldException) {
      e.printStackTrace()
    } catch (e: IllegalAccessException) {
      e.printStackTrace()
    }
  }

}