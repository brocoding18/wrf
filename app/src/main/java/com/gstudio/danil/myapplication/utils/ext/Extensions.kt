package com.gstudio.danil.myapplication.utils.ext

import android.content.Context
import android.support.design.widget.Snackbar
import android.support.v4.app.FragmentActivity
import android.support.v7.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import com.gstudio.danil.myapplication.Constants.GENERAL_TEXT_SIZE
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity

fun BaseActivity.hideKeyboard() {
  try {
    this.window.setSoftInputMode(
        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN
    )
    if (this.currentFocus != null && this.currentFocus!!.windowToken != null) {
      (this.getSystemService(
          Context.INPUT_METHOD_SERVICE
      ) as InputMethodManager).hideSoftInputFromWindow(
          this.currentFocus!!.windowToken, 0
      )
    }
  } catch (e: Exception) {
    e.printStackTrace()
  }
}

fun showSnackbarWithMessage(activity: AppCompatActivity, layoutId: Int, text: String) {
  val view: View = activity.findViewById(layoutId)

  val snackbar = Snackbar.make(view, text, Snackbar.LENGTH_SHORT)

  val snackbarView = snackbar.view
  snackbarView.background = activity.resources.getDrawable(R.color.black, null)

  val snackbarTextView =
    snackbar.view.findViewById(android.support.design.R.id.snackbar_text) as (TextView)

  snackbarTextView.apply {
    textAlignment = View.TEXT_ALIGNMENT_CENTER
    setTextAppearance(R.style.fontForBasisBold)
    textSize = GENERAL_TEXT_SIZE
  }

  snackbar.show()

}

fun EditText.showKeyboard(activity: FragmentActivity) {
  activity.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE or WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)

  this.requestFocus()
  (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
      .toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun ViewGroup.inflate(layoutId: Int): View = LayoutInflater.from(context).inflate(layoutId, this, false)

fun View.setBackgroundColorExt(color: Int) = this.setBackgroundColor(context.resources.getColor(color, null))

fun hideAndShowViews(viewsToGone: List<View>? = null, viewsToHide: List<View>? = null, viewsToShow: List<View>? = null) {
  viewsToGone?.let { viewsToGone.forEach { it.visibility = View.GONE } }
  viewsToHide?.let { viewsToHide.forEach { it.visibility = View.INVISIBLE } }
  viewsToShow?.let { viewsToShow.forEach { it.visibility = View.VISIBLE } }
}