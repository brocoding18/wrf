package com.gstudio.danil.myapplication.ui.splash

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.util.Pair
import android.view.View
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import com.gstudio.danil.myapplication.ui.login.LoginActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class SplashActivity : BaseActivity(), SplashView {

  @Inject override lateinit var navigator: SupportAppNavigator
  @Inject lateinit var presenter: SplashPresenterImp

  @BindView(R.id.splash_logo) lateinit var splashLogo: ImageView

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.splash)
    ButterKnife.bind(this)

  }

  @OnClick(R.id.splash_logo)
  fun layoutOnClick() {
    val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
        this,
        Pair<View, String>(
            splashLogo,
            LoginActivity.VIEW_LOGO_HEADER_IMAGE
        )
    )

    val intent = Intent(this, LoginActivity::class.java)
    startActivity(intent, activityOptions.toBundle())
  }

  // TODO надо использовать роутер и навигатор.
  override fun toLogin() {
    val activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
        this,
        Pair<View, String>(
            splashLogo,
            LoginActivity.VIEW_LOGO_HEADER_IMAGE
        )
    )

    val intent = Intent(this, LoginActivity::class.java)
    startActivity(intent, activityOptions.toBundle())
  }
}
