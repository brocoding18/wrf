package com.gstudio.danil.myapplication.ui.check_list.fragments.roles

interface ChecklistRolesPresenter {
  fun getRolesList()
}