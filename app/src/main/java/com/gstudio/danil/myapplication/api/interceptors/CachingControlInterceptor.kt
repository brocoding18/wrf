package com.gstudio.danil.myapplication.api.interceptors

import com.gstudio.danil.myapplication.sp.SpHelper
import okhttp3.CacheControl
import okhttp3.Interceptor
import okhttp3.Response
import java.util.concurrent.TimeUnit

class CachingControlInterceptor(val spHelper: SpHelper) : Interceptor {

  override fun intercept(chain: Interceptor.Chain): Response {

    val requestBuilder = chain.request()
        .newBuilder()
    val cacheControl = CacheControl.Builder()
        .maxStale(1, TimeUnit.MINUTES)
        .maxAge(1, TimeUnit.MINUTES)
        .build()

    val token = spHelper.getToken()

    if (token != null) {
      requestBuilder.addHeader("Authorization", "Bearer $token")
    }

    requestBuilder.cacheControl(cacheControl)
    // TODO Внимательнее!!! Ты это закомментил для теста хэдеров
//    requestBuilder.header("Content-Type", "application/json")
    val request = requestBuilder.build()

    val originalResponse = chain.proceed(request)
    return originalResponse.newBuilder()
        .removeHeader("Pragma")
        .removeHeader("Cache-Control")
        .header("Cache-Control", "public, only-if-cached, max-stale=604800")
        .build()
  }
}