package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class WorkingDayPreviousListsDTO(
  @SerializedName("data") val workingDayPrevious: List<WorkingDayPreviousList>,
  @SerializedName("pages") val pagesTotal: Int
)

data class WorkingDayPreviousList(
  @SerializedName("id") val id: Int,
  @SerializedName("user_opened") val userOpened: WorkingDayUser,
  @SerializedName("user_closed") val userClosed: WorkingDayUser,
  @SerializedName("created_at") val createdAt: String?,
  @SerializedName("closed_at") val closedAt: String?
)