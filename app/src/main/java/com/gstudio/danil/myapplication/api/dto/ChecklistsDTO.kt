package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class ChecklistsDTO(
  @SerializedName("data") val data: ArrayList<Checklist>
)

data class Checklist(
  @SerializedName("id") val id: Int,
  @SerializedName("title") val title: String,
  @SerializedName("subtitle") val subtitle: String,
  @SerializedName("description") val description: String,
  @SerializedName("done_at") var  doneAt: String,
  @SerializedName("user_check_list") var userCheckList: UserChecklistStatus?
)

data class UserChecklistStatus(
  @SerializedName("id") val id: Int,
  @SerializedName("created_at") var createdAt: String,
  @SerializedName("status") var status: String
)

data class ChecklistPatchRequest(
  @SerializedName("status") val status: String
)