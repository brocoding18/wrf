package com.gstudio.danil.myapplication.ui.innovations

interface InnovationsPresenter {
  fun onBackCommandClick()
  fun firstScreen()
}