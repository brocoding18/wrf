package com.gstudio.danil.myapplication.utils

import android.support.v7.widget.RecyclerView
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.entities.Category
import com.gstudio.danil.myapplication.utils.SingleTextViewAdapter.ProfileHolder
import com.gstudio.danil.myapplication.utils.ext.inflate

class SingleTextViewAdapter(
  private val optionsList: List<Category>,
  private val listener: OnItemClickListener?
) : RecyclerView.Adapter<ProfileHolder>() {

  override fun onCreateViewHolder(
    container: ViewGroup,
    viewType: Int
  ): ProfileHolder = ProfileHolder(container.inflate(R.layout.utils_single_textview_item), listener)

  override fun getItemCount(): Int = optionsList.size

  override fun onBindViewHolder(
    holder: ProfileHolder,
    position: Int
  ) {
    val optionsItem = optionsList[position]
    holder.bindItem(optionsItem)
  }

  interface OnItemClickListener {
    fun onItemClick(optionsItem: Category)
  }

  class ProfileHolder(itemView: View, listener: OnItemClickListener?)
    : RecyclerView.ViewHolder(itemView) {

    private lateinit var optionsItem: Category
    @BindView(R.id.options_item) lateinit var optionsItemView: TextView

    init {
      itemView.setOnClickListener {
        if (listener != null && adapterPosition != RecyclerView.NO_POSITION) {
          listener.onItemClick(optionsItem)
        }
      }

      ButterKnife.bind(this, itemView)
    }

    fun bindItem(optionsItem: Category) {
      this.optionsItem = optionsItem

      optionsItemView.text = optionsItem.name
      if (this.optionsItem.layoutType.type == "Menu") {
        // TODO нужно оптимизировать стили
        // Для подобных вещей нужен API 26
//        val typeface = mContext.resources.getFont(R.font.basis_grotesque_pro_regular)
//        optionsItemView.setTypeface(typeface, Typeface.NORMAL)
        //с
        optionsItemView.apply {
          setTextAppearance(R.style.fontForBasisBold)
          gravity = Gravity.CENTER
        }
      }
    }
  }
}