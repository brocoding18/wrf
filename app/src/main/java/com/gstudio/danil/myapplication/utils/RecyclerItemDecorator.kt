package com.gstudio.danil.myapplication.utils

import android.graphics.Canvas
import android.graphics.Rect
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.State
import android.view.View

class RecyclerItemDecorator(
  private val padBottom: Float,
  private val horizontalDivider: Drawable? = null
) : RecyclerView.ItemDecoration() {

  override fun getItemOffsets(
    outRect: Rect,
    view: View,
    parent: RecyclerView,
    state: State
  ) {
    super.getItemOffsets(outRect, view, parent, state)

    val position = parent.getChildLayoutPosition(view)

    if (position != RecyclerView.NO_POSITION) {
      if (parent.getChildAdapterPosition(view) == state.itemCount - 1) {
        outRect.bottom = (padBottom).toInt()
      } else if (horizontalDivider != null) {
        outRect.bottom = horizontalDivider.intrinsicHeight
      }
    }
  }

  override fun onDraw(c: Canvas, parent: RecyclerView, state: State) {
    if (horizontalDivider == null) {
      super.onDraw(c, parent, state)
      return
    }

    val dividerLeft = parent.paddingLeft
    val dividerRight = parent.width - parent.paddingRight

    val childCount = parent.childCount
    for (i in 0 until childCount) {
      val child = parent.getChildAt(i)

      val params = child.layoutParams as RecyclerView.LayoutParams

      val dividerTop = child.bottom + params.bottomMargin
      val dividerBottom = dividerTop + horizontalDivider.intrinsicHeight

      horizontalDivider.apply {
        setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom)
        draw(c)
      }
    }
  }
}