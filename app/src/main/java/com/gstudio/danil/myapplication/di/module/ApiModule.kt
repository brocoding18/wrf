package com.gstudio.danil.myapplication.di.module

import com.gstudio.danil.myapplication.api.ApiServiceHolder
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerApplication
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApiModule {

  @Provides
  @PerApplication
  fun providesEndpoints(
    retrofit: Retrofit,
    apiServiceHolder: ApiServiceHolder
  ): Endpoints {
    val endpoints = retrofit.create(Endpoints::class.java)
    apiServiceHolder.apiService = endpoints
    return endpoints
  }

  @Provides
  @PerApplication
  fun provideApiServiceHolder(): ApiServiceHolder = ApiServiceHolder()

}