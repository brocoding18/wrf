package com.gstudio.danil.myapplication.ui.working_day_log.fragments.add_event

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventRequest
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.utils.ext.hideKeyboard
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import com.gstudio.danil.myapplication.utils.ext.showKeyboard
import javax.inject.Inject

class AddFragment : BaseFragment(), AddView {
  companion object {
    fun newInstance(): AddFragment = AddFragment()
  }

  @BindView(R.id.util_general_header) lateinit var headerText: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar_right_side_text) lateinit var headerRightSideText: TextView
  @BindView(R.id.util_description_button) lateinit var sendMessageButton: Button
  @BindView(R.id.util_description_message) lateinit var messageText: EditText
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout

  @Inject lateinit var presenter: AddEventPresenterImp

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = inflater.inflate(R.layout.util_description_fragment, container, false)
    ButterKnife.bind(this, view)

    headerText.text = "Текст события"
    headerDetail.text = "Опишите максимально детально ваше событие"
    sendMessageButton.text = "Добавить событие"
    headerRightSideText.text = "Борис"

    headerBackButton.setOnClickListener { onUpButtonClick() }
    headerRightSideText.setOnClickListener { presenter.revealProfile() }

    return view
  }

  override fun onResume() {
    super.onResume()

    messageText.showKeyboard(activity!!)
  }

  override fun onPause() {
    super.onPause()
    hideKeyboard()
  }

  override fun onEventSent() {
    onUpButtonClick()
  }

  @OnClick(R.id.util_description_button)
  fun onAddButtonClick() {
    presenter.addEvent(WorkingDayEventRequest(event_text = messageText.text.toString()))
  }


}