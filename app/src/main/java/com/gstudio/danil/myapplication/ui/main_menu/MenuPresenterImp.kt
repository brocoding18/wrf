package com.gstudio.danil.myapplication.ui.main_menu

import com.gstudio.danil.myapplication.Screens.ChecklistActivityScreen
import com.gstudio.danil.myapplication.Screens.InnovationActivityScreen
import com.gstudio.danil.myapplication.Screens.ProfileActivityScreen
import com.gstudio.danil.myapplication.Screens.WorkingDayScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class MenuPresenterImp @Inject constructor(
  val menuView: MenuView,
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<MenuView>(router), MenuPresenter {

  override fun onBackCommandClick() {
    // TODO надо бы это куда- то впилить
//    router.exit()
    println("BEDAAAAAAAAAAAAAAAAAAAAAA")
  }

  override fun onInnovations() {
    router.navigateTo(InnovationActivityScreen)
  }

  override fun onChecklist() {
    router.navigateTo(ChecklistActivityScreen)
  }

  override fun onWorkingDay() {
    router.navigateTo(WorkingDayScreen)
  }

  fun onMenuAvatarClick() {
    router.navigateTo(ProfileActivityScreen)
  }
}