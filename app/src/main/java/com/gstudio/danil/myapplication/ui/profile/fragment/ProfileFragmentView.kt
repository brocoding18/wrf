package com.gstudio.danil.myapplication.ui.profile.fragment

import com.gstudio.danil.myapplication.mvp.BaseView

interface ProfileFragmentView : BaseView {
  fun putDataToViews(userName: String?, imageUrl: String?, restaurantName: String?, function: String?, checklistsDone: Int, checklistsTotal: Int)
}