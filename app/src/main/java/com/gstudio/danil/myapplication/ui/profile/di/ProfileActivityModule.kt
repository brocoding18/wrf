package com.gstudio.danil.myapplication.ui.profile.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.ui.profile.ProfileActivity
import com.gstudio.danil.myapplication.ui.profile.ProfileActivityPresenter
import com.gstudio.danil.myapplication.ui.profile.ProfileActivityPresenterImp
import com.gstudio.danil.myapplication.ui.profile.ProfileView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class ProfileActivityModule {

  @Provides
  @PerActivity
  fun provideProfileView(profileActivity: ProfileActivity): ProfileView = profileActivity

  @Provides
  @PerActivity
  fun provideProfileNavigator(activity: ProfileActivity): SupportAppNavigator =
    SupportAppNavigator(activity, R.id.fragment_container)

  @Provides
  @PerActivity
  fun providePresenter(
    apiService: Endpoints,
    router: Router
  ): ProfileActivityPresenter = ProfileActivityPresenterImp(apiService, router)

}