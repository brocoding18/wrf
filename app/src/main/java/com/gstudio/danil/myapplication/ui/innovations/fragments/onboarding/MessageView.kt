package com.gstudio.danil.myapplication.ui.innovations.fragments.onboarding

import com.gstudio.danil.myapplication.mvp.BaseView

interface MessageView : BaseView {
  fun onLayoutClick()
}