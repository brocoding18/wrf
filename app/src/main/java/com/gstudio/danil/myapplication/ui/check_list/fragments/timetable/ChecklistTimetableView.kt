package com.gstudio.danil.myapplication.ui.check_list.fragments.timetable

import com.gstudio.danil.myapplication.mvp.BaseView

interface ChecklistTimetableView : BaseView {
  fun setupAdapter()
}