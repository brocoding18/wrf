package com.gstudio.danil.myapplication.ui.working_day_log

import android.os.Bundle
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class WorkingDayActivity : BaseActivity(), WorkingDayView {

  @Inject lateinit var presenter: WorkingDayPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.utils_activity_fragment)

    presenter.notebookScreen()
  }
}
