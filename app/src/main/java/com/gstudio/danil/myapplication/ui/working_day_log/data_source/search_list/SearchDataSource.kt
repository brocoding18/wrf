package com.gstudio.danil.myapplication.ui.working_day_log.data_source.search_list

import android.arch.paging.PageKeyedDataSource
import android.content.res.Resources
import com.gstudio.danil.myapplication.Constants.WorkingDay.SEARCH_STRING
import com.gstudio.danil.myapplication.Constants.WorkingDay.SEARCH_TOTAL_PAGES
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.WorkingDayEventsDTO
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayNotebookItem
import com.gstudio.danil.myapplication.utils.ext.formattedTime
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SearchDataSource @Inject constructor(
  val resources: Resources,
  val apiService: Endpoints,
  val disposable: CompositeDisposable
) : PageKeyedDataSource<Int, WorkingDayItem>() {

  private lateinit var searchResponse: Single<WorkingDayEventsDTO>

  override fun loadInitial(
    params: LoadInitialParams<Int>,
    callback: LoadInitialCallback<Int, WorkingDayItem>
  ) {
    searchResponse = createObservable(params, null)

    disposable.add(
      searchResponse.subscribe({ response: WorkingDayEventsDTO ->
        val result: MutableList<WorkingDayItem> = ArrayList()

        SEARCH_TOTAL_PAGES = response.pages

        response.workingDayEvents.forEach {
          val timeAndPerson = resources.getString(
            R.string.working_day_item_time_person, formattedTime(it.createdAt), it.user.name
          )

          result.add(
            WorkingDayNotebookItem(
              id = it.id,
              timeAndPerson = timeAndPerson,
              text = it.eventText
            )
          )
        }

        callback.onResult(result, null, 2)
      },
        { e: Throwable -> })
    )
  }

  override fun loadAfter(
    params: LoadParams<Int>,
    callback: LoadCallback<Int, WorkingDayItem>
  ) {
    if (params.key > SEARCH_TOTAL_PAGES || params.key == null) {
      disposable.clear()
      callback.onResult(ArrayList(), null)
      return
    }

    searchResponse = createObservable(null, params)

    disposable.add(
      searchResponse.subscribe({ response: WorkingDayEventsDTO ->
        val result: MutableList<WorkingDayItem> = ArrayList()

        SEARCH_TOTAL_PAGES = response.pages

        response.workingDayEvents.forEach {
          val timeAndPerson = resources.getString(
            R.string.working_day_item_time_person, formattedTime(it.createdAt), it.user.name
          )

          result.add(
            WorkingDayNotebookItem(
              id = it.id,
              timeAndPerson = timeAndPerson,
              text = it.eventText
            )
          )
        }

        callback.onResult(result, params.key + 1)
      },
        { e: Throwable ->
          println(e)
        }))
  }


  // кажется, мы не должны выполнять работу в этом методе
  override fun loadBefore(
    params: LoadParams<Int>,
    callback: LoadCallback<Int, WorkingDayItem>
  ) {
    if (params.key < 1 || params.key == null) {
      disposable.clear()
      callback.onResult(ArrayList(), null)
      return
    }


  }

  private fun createObservable(
    loadInitialParams: PageKeyedDataSource.LoadInitialParams<Int>?,
    loadRangeParams: PageKeyedDataSource.LoadParams<Int>?
  ): Single<WorkingDayEventsDTO> =
    if (loadInitialParams != null) {
      createInitialDataRequest()
    } else {
      createPaginationRequest(loadRangeParams!!.key)
    }

  private fun createInitialDataRequest(): Single<WorkingDayEventsDTO> =
    apiService.getSearchResult(SEARCH_STRING, 1)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())

  private fun createPaginationRequest(key: Int): Single<WorkingDayEventsDTO> =
    apiService.getSearchResult(SEARCH_STRING, key)
        .subscribeOn(Schedulers.newThread())
        .observeOn(AndroidSchedulers.mainThread())
}
