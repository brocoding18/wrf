package com.gstudio.danil.myapplication.ui.working_day_log.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.holder.WorkingDayNotebookHolder
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.utils.ext.inflate

class WorkingDayDetailAdapter(
  private val list: ArrayList<WorkingDayItem>
) : RecyclerView.Adapter<WorkingDayNotebookHolder>() {
  override fun onCreateViewHolder(container: ViewGroup, viewType: Int): WorkingDayNotebookHolder =
    WorkingDayNotebookHolder(container.inflate(R.layout.working_day_notebook_item))

  override fun getItemCount(): Int = list.size

  override fun onBindViewHolder(holder: WorkingDayNotebookHolder, position: Int) {
    holder.bindItem(list[position])
  }
}