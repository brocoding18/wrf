package com.gstudio.danil.myapplication.api.dto

import com.google.gson.annotations.SerializedName

data class InnovationsPhotoPostDTO(
  @SerializedName("id") val id: Int,
  @SerializedName("user_id")  val user_id: Int,
  @SerializedName("category_id")  val category_id: Int,
  @SerializedName("idea")  val idea: String,
  @SerializedName("images")  val images: List<InnovationsPhotos>
)

data class InnovationsPhotos(
  @SerializedName("id") val id: Int,
  @SerializedName("url") val url: String
)