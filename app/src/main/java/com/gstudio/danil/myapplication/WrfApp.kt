package com.gstudio.danil.myapplication

import com.facebook.stetho.Stetho
import com.gstudio.danil.myapplication.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import net.danlew.android.joda.JodaTimeAndroid

class WrfApp : DaggerApplication() {
  /*companion object {
    lateinit var INSTANCE: WrfApp
  }*/

  override fun applicationInjector(): AndroidInjector<out DaggerApplication>? =
    DaggerAppComponent.builder().create(this)

  override fun onCreate() {
    super.onCreate()
    JodaTimeAndroid.init(this)
    Stetho.initializeWithDefaults(this)

//    INSTANCE = this
  }
}