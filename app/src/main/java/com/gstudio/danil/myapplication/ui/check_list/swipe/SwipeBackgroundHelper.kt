package com.gstudio.danil.myapplication.ui.check_list.swipe

import android.graphics.*
import android.support.v4.content.ContextCompat
import android.view.View
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.R.color

class SwipeBackgroundHelper {

  companion object {
    private const val THRESHOLD = 2

    private lateinit var textStyle: Typeface
    private val textPaint = Paint(Paint.LINEAR_TEXT_FLAG or Paint.ANTI_ALIAS_FLAG)

    @JvmStatic
    fun paintDrawCommandToStart(
      canvas: Canvas,
      viewItem: View,
      dX: Float
    ) {
      // TODO стоит делать это при ИНИТЕ объекта
      textStyle =
          Typeface.createFromAsset(viewItem.context.assets, "fonts/basis_grotesque_pro_bold.ttf")
      textPaint.apply {
        color = Color.WHITE
        textSize = viewItem.context.resources.getDimension(R.dimen.general_title_text_size)
        typeface =
            textStyle
      }

      val drawCommand =
        createDrawCommand(
            viewItem, dX
        )
      paintDrawCommand(
          drawCommand, canvas, dX, viewItem
      )
    }

    private fun createDrawCommand(
      viewItem: View,
      dX: Float
    ): DrawCommand {

      val context = viewItem.context
      val statusText: String
      val backgroundColor: Int

      if (dX >= 0) {
        backgroundColor =
            getBackgroundColor(
                color.green, color.black, dX, viewItem
            )
        statusText = context.resources.getString(R.string.checklist_status_done)
      } else {
        backgroundColor =
            getBackgroundColor(
                color.crimson, color.black, dX, viewItem
            )
        statusText = context.resources.getString(R.string.checklist_status_undone)
      }

      return DrawCommand(
          statusText, backgroundColor
      )
    }

    private fun getBackgroundColor(
      firstColor: Int,
      secondColor: Int,
      dX: Float,
      viewItem: View
    ): Int = when (willActionBeTriggered(
        dX, viewItem.width
    )) {
      true -> ContextCompat.getColor(viewItem.context, firstColor)
      false -> ContextCompat.getColor(viewItem.context, secondColor)
    }

    private fun paintDrawCommand(
      drawCommand: DrawCommand,
      canvas: Canvas,
      dX: Float,
      viewItem: View
    ) {
      drawBackground(
          canvas, viewItem, dX, drawCommand.backgroundColor
      )
      drawStatusText(
          canvas, viewItem, dX, drawCommand.statusText
      )
    }

    private fun drawStatusText(
      canvas: Canvas,
      viewItem: View,
      dX: Float,
      statusText: String
    ) =
      dynamicTextDraw(
          viewItem, dX, statusText, canvas
      )

    private fun dynamicTextDraw(
      viewItem: View,
      dX: Float,
      statusText: String,
      canvas: Canvas
    ) {
      val leftBound: Float
      val rightBound: Float
      val y: Float = (viewItem.bottom - viewItem.height / 2).toFloat()

      if (dX <= 0) {
        // Отрисовка текста при свайпе справа-налево
        rightBound = viewItem.right + (dX / 5)
        textPaint.textAlign = Paint.Align.RIGHT
        canvas.drawText(statusText, rightBound, y,
            textPaint
        )
      } else {
        // Отрисовка текста при свайпе слева-направо
        textPaint.textAlign = Paint.Align.RIGHT
        val textWidth = textPaint.measureText(statusText)
        leftBound = viewItem.left + (dX / 5) + textWidth
        canvas.drawText(statusText, leftBound, y,
            textPaint
        )
      }
    }

    private fun drawBackground(
      canvas: Canvas,
      viewItem: View,
      dX: Float,
      color: Int
    ) {
      val backgroundPaint = Paint(Paint.ANTI_ALIAS_FLAG)
      backgroundPaint.color = color
      val backgroundRectangle =
        getBackGroundRectangle(
            viewItem, dX
        )
      canvas.drawRect(backgroundRectangle, backgroundPaint)
    }

    private fun getBackGroundRectangle(
      viewItem: View,
      dX: Float
    ): RectF = if (dX <= 0) {
      // Отрисовка бэкграунда при свайпе справа-налево
      RectF(
          viewItem.right.toFloat() + dX, viewItem.top.toFloat(), viewItem.right.toFloat(),
          viewItem.bottom.toFloat()
      )
    } else {
      // Отрисовка бэкграунда при свайпе слева-направо
      RectF(
          viewItem.left.toFloat(), viewItem.top.toFloat(), viewItem.right.toFloat() + dX,
          viewItem.bottom.toFloat()
      )
    }

    private fun willActionBeTriggered(
      dX: Float,
      viewWidth: Int
    ): Boolean = Math.abs(dX) >= viewWidth / THRESHOLD

  }

  private class DrawCommand constructor(
    val statusText: String,
    val backgroundColor: Int
  )
}
