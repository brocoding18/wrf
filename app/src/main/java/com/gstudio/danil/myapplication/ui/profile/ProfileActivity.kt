package com.gstudio.danil.myapplication.ui.profile

import android.os.Bundle
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseActivity
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import javax.inject.Inject

class ProfileActivity : BaseActivity(), ProfileView {

  @Inject lateinit var presenter: ProfileActivityPresenterImp
  @Inject override lateinit var navigator: SupportAppNavigator

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.utils_activity_fragment)

    presenter.revealProfileScreen()
  }
}