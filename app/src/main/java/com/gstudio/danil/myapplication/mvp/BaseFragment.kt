package com.gstudio.danil.myapplication.mvp

import android.os.Bundle
import dagger.android.support.DaggerFragment

abstract class BaseFragment: DaggerFragment(), BaseView {

  private var presenter: BasePresenter<*>? = null

  override fun setPresenter(presenter: BasePresenter<*>) {
    this.presenter = presenter
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setHasOptionsMenu(true)
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter?.detachView()
    presenter = null
  }
}