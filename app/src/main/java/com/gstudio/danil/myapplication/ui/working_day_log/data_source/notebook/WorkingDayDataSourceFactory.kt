package com.gstudio.danil.myapplication.ui.working_day_log.data_source.notebook

import android.arch.paging.DataSource
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import javax.inject.Inject

class WorkingDayDataSourceFactory @Inject constructor(private val workingDayDataSource: WorkingDayDataSource) :
    DataSource.Factory<Int, WorkingDayItem>() {

  override fun create(): DataSource<Int, WorkingDayItem> = workingDayDataSource
}