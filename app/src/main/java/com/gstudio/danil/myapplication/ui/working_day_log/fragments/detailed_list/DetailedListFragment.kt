package com.gstudio.danil.myapplication.ui.working_day_log.fragments.detailed_list

import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.mvp.BaseFragment
import com.gstudio.danil.myapplication.ui.working_day_log.adapter.WorkingDayDetailAdapter
import com.gstudio.danil.myapplication.ui.working_day_log.data_source.WorkingDayItem
import com.gstudio.danil.myapplication.utils.RecyclerItemDecorator
import com.gstudio.danil.myapplication.utils.ext.hideAndShowViews
import com.gstudio.danil.myapplication.utils.ext.inflate
import com.gstudio.danil.myapplication.utils.ext.onUpButtonClick
import javax.inject.Inject

class DetailedListFragment : BaseFragment(), DetailedListView {
  companion object {
    private const val ARG_WORKING_DAY_DETAIL_ID = "working_day_shift_id"
    private const val ARG_WORKING_DAY_DETAIL_PERSON = "working_day_shift_person"
    private const val ARG_WORKING_DAY_DETAIL_CLOSE_DATE = "working_day_shift_close_date"

    fun newInstance(shiftId: Int, person: String?, closeDate: String?): DetailedListFragment {
      val args = Bundle()
      args.putInt(ARG_WORKING_DAY_DETAIL_ID, shiftId)
      args.putString(ARG_WORKING_DAY_DETAIL_PERSON, person)
      args.putString(ARG_WORKING_DAY_DETAIL_CLOSE_DATE, closeDate)

      val fragment = DetailedListFragment()
      fragment.arguments = args
      return fragment
    }
  }

  @BindView(R.id.util_general_header) lateinit var header: TextView
  @BindView(R.id.util_general_header_detail_message) lateinit var headerDetail: TextView
  @BindView(R.id.util_toolbar) lateinit var toolbar: Toolbar
  @BindView(R.id.util_fragment_recycler) lateinit var recyclerView: RecyclerView
  @BindView(R.id.util_toolbar_right_side_text) lateinit var toolbarRightSideText: TextView
  @BindView(R.id.util_toolbar_back_button_container) lateinit var headerBackButton: ConstraintLayout

  @BindView(R.id.util_fragment_progress_bar) lateinit var progressBar: ProgressBar

  @Inject lateinit var presenter: DetailPresenterImp

  private var shiftId: Int? = null
  private var personName: String? = null
  private var closeDate: String? = null

  override fun onCreateView(
    inflater: LayoutInflater,
    container: ViewGroup?,
    savedInstanceState: Bundle?
  ): View? {
    val view = container!!.inflate(R.layout.util_header_and_recycler_view)
    ButterKnife.bind(this, view)

    unboxArgs()
    setupToolbar()

    return view
  }

  override fun onResume() {
    super.onResume()

    presenter.getDetailList(shiftId!!)
  }

  private fun unboxArgs() = with(arguments!!) {
    shiftId = getInt(ARG_WORKING_DAY_DETAIL_ID)
    personName = getString(ARG_WORKING_DAY_DETAIL_PERSON)
    closeDate = getString(ARG_WORKING_DAY_DETAIL_CLOSE_DATE)
  }

  private fun setupToolbar() {
    header.text = "Журнал Смены"
    headerDetail.text = activity!!.resources.getString(R.string.working_day_detailed_list_header, closeDate, personName)

    hideAndShowViews(viewsToGone = listOf(toolbarRightSideText))

    headerBackButton.setOnClickListener { onUpButtonClick() }
  }

  override fun setupAdapter(list: ArrayList<WorkingDayItem>) {
    val currentAdapter = WorkingDayDetailAdapter(list)

    recyclerView.apply {
      adapter = currentAdapter
      layoutManager = LinearLayoutManager(activity!!)
      addItemDecoration(RecyclerItemDecorator(resources.getDimension(R.dimen.working_day_notebook_bottom_padding))
      )
    }

    hideAndShowViews(viewsToHide = listOf(progressBar), viewsToShow = listOf(recyclerView))
  }
}