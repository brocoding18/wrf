package com.gstudio.danil.myapplication.di

import android.app.Application
import com.gstudio.danil.myapplication.WrfApp
import com.gstudio.danil.myapplication.ui.check_list.ChecklistActivity
import com.gstudio.danil.myapplication.ui.check_list.di.ChecklistActivityModule
import com.gstudio.danil.myapplication.ui.check_list.di.ChecklistFragmentProvider
import com.gstudio.danil.myapplication.ui.innovations.InnovationsActivity
import com.gstudio.danil.myapplication.ui.innovations.di.InnovationsActivityModule
import com.gstudio.danil.myapplication.ui.innovations.di.InnovationsFragmentProvider
import com.gstudio.danil.myapplication.ui.login.LoginActivity
import com.gstudio.danil.myapplication.ui.login.di.LoginActivityModule
import com.gstudio.danil.myapplication.ui.main_menu.MenuActivity
import com.gstudio.danil.myapplication.ui.main_menu.di.MenuActivityModule
import com.gstudio.danil.myapplication.ui.profile.ProfileActivity
import com.gstudio.danil.myapplication.ui.profile.di.ProfileActivityModule
import com.gstudio.danil.myapplication.ui.profile.di.ProfileFragmentProvider
import com.gstudio.danil.myapplication.ui.splash.SplashActivity
import com.gstudio.danil.myapplication.ui.splash.di.SplashActivityModule
import com.gstudio.danil.myapplication.ui.working_day_log.WorkingDayActivity
import com.gstudio.danil.myapplication.ui.working_day_log.di.WorkingDayActivityModule
import com.gstudio.danil.myapplication.ui.working_day_log.di.WorkingDayFragmentProvider
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

  @Binds
  abstract fun bindApplication(wrfApp: WrfApp): Application

  @PerActivity
  @ContributesAndroidInjector(modules = [LoginActivityModule::class])
  abstract fun bindLoginActivity(): LoginActivity

  @PerActivity
  @ContributesAndroidInjector(modules = [SplashActivityModule::class])
  abstract fun bindSplashActivity(): SplashActivity

  @PerActivity
  @ContributesAndroidInjector(modules = [MenuActivityModule::class])
  abstract fun bindMenuActivity(): MenuActivity

  @PerActivity
  @ContributesAndroidInjector(modules = [InnovationsActivityModule::class, InnovationsFragmentProvider::class])
  abstract fun bindInnovationsActivity(): InnovationsActivity

  @PerActivity
  @ContributesAndroidInjector(modules = [ChecklistActivityModule::class, ChecklistFragmentProvider::class])
  abstract fun bindChecklistActivity(): ChecklistActivity

  @PerActivity
  @ContributesAndroidInjector(modules = [WorkingDayActivityModule::class, WorkingDayFragmentProvider::class])
  abstract fun bindWorkingDayActivity(): WorkingDayActivity


  @PerActivity
  @ContributesAndroidInjector(modules = [ProfileActivityModule::class, ProfileFragmentProvider::class])
  abstract fun bindProfileActivity(): ProfileActivity

}