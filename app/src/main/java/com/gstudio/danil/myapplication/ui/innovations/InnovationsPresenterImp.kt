package com.gstudio.danil.myapplication.ui.innovations

import com.gstudio.danil.myapplication.Screens.InnovationsMessageScreen
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.api.dto.OnboardingDTO
import com.gstudio.danil.myapplication.mvp.BasePresenter
import com.gstudio.danil.myapplication.sp.SpHelper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import javax.inject.Inject

class InnovationsPresenterImp @Inject constructor(
  val apiService: Endpoints,
  val router: Router,
  val spHelper: SpHelper
) : BasePresenter<InnovationsView>(router), InnovationsPresenter {

  // TODO как- то нужно это прикрутить
  override fun onBackCommandClick() = router.exit()

  override fun firstScreen() {
    disposable.add(
      apiService.getInnovationsOnboarding()
          .subscribeOn(Schedulers.newThread())
          .observeOn(AndroidSchedulers.mainThread())
          .subscribe({ response: OnboardingDTO ->

            router.newRootChain(InnovationsMessageScreen(response.text))
          },
            { e: Throwable ->
              val errorBody = (e as HttpException).response()
                  .errorBody()
              println(errorBody)
            }))
  }
}