package com.gstudio.danil.myapplication.ui.splash.di

import com.gstudio.danil.myapplication.R
import com.gstudio.danil.myapplication.api.Endpoints
import com.gstudio.danil.myapplication.di.PerActivity
import com.gstudio.danil.myapplication.ui.splash.SplashActivity
import com.gstudio.danil.myapplication.ui.splash.SplashPresenter
import com.gstudio.danil.myapplication.ui.splash.SplashPresenterImp
import com.gstudio.danil.myapplication.ui.splash.SplashView
import dagger.Module
import dagger.Provides
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.android.support.SupportAppNavigator

@Module
class SplashActivityModule {

  @Provides
  @PerActivity
  fun provideSplashView(splashActivity: SplashActivity): SplashView = splashActivity

  @Provides
  @PerActivity
  fun provideSplashNavigator(activity: SplashActivity): SupportAppNavigator = SupportAppNavigator(activity, R.id.fragment_container)

  // TODO временно просим провайдить okhttpBuilder и requestInterceptor, пока сервер не присылает
  // TODO сообщение о том, что токен стух
  @Provides
  @PerActivity
  fun provideSplashPresenter(
    splashView: SplashView,
    router: Router,
    apiService: Endpoints
  ): SplashPresenter =
    SplashPresenterImp(splashView, router, apiService)


}