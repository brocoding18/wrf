package com.gstudio.danil.myapplication.ui.innovations.fragments.categories

import com.gstudio.danil.myapplication.mvp.BaseView

interface CategoriesView : BaseView {
  fun setupAdapter()
}