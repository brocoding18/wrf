package com.gstudio.danil.myapplication.api.dto

data class RefreshRequest(
  val token: String
)