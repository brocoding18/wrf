package dependecies

object Versions {
  val versionName = "1.0"
  val versionCode = 1
  object SupportAndroidLibs {
    const val gradlePlugin = "3.0.1"
    const val servicesPlugin = "3.2.0"
    const val compileSdk = 28
    const val minSdk = 23
    const val targetSdk = 28
//    const val buildTools = "27.0.3"
    const val supportLibrary = "28.0.0"
    const val multiDex = "1.0.3"
    const val constraintLayout = "1.1.3"
//    const val androidArcComponents = "1.1.1"
  }
  object Testing {
    const val mockito = "2.10.0"
    const val espresso = "3.0.1"
    const val runner = "1.0.1"
    const val junit = "4.12"
    const val junitPlatform = "1.0.0"
    const val spek = "1.1.5"
  }
  object Kotlin {
    const val std = "1.3.0"
  }
  /*object Google {
    const val playServices = "12.0.1"
    const val firebase = "12.0.1"
    const val dagger = "2.14.1"
  }*/
  object Libraries {
    // Retrofit + OkHttp + Gson
    const val retrofit = "2.4.0"
    const val okHttp = "3.11.0"
    // Stetho
    const val stetho = "1.5.0"
    // RxJava and RxAndroid
    const val rxAndroid = "2.0.2"
    const val rxJava = "2.2.0"
    const val rxRelay = "2.0.0"
    const val rxIdler = "0.9.0"
    // Dagger2
    const val dagger = "2.19"

    // Room for Database
    const val room = "1.1.1"
    // Paging Library
    const val paging = "1.0.0"
    // Timber
    const val timber = "1.5.1"
    // ButterKnife
    const val butterknife = "8.8.1"
    const val butterknifeCompiler= "8.5.1"
    //LeakCanary
    const val leakCanary = "1.6.2"
    // Glide
    const val glide = "4.7.1"
  }
}