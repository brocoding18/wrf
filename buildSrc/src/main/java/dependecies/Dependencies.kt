package dependecies

object Dependencies {
  // All The Support Android Libraries are grouped in this supportAndroidLibs array
  val supportAndroidLibs = arrayOf(
      "com.android.support:appcompat-v7:${Versions.SupportAndroidLibs.supportLibrary}",
      "com.android.support:cardview-v7:${Versions.SupportAndroidLibs.supportLibrary}",
      "com.android.support:recyclerview-v7:${Versions.SupportAndroidLibs.supportLibrary}",
      "com.android.support:design:${Versions.SupportAndroidLibs.supportLibrary}",
      "com.android.support.constraint:constraint-layout:${Versions.SupportAndroidLibs.constraintLayout}"
  )
  // The same here in Kotlin Libraries
  val kotlin = arrayOf(
      "org.jetbrains.kotlin:kotlin-stdlib-jdk7:${Versions.Kotlin.std}"
  )
  // The same here in Google Libraries
  /*val google = arrayOf(
      "com.google.android.gms:play-services-analytics:${Versions.Google.playServices}",
      "com.google.firebase:firebase-core:${Versions.Google.firebase}",
      "com.google.dagger:dagger:${Versions.Google.dagger}",
      "com.google.dagger:dagger-android:${Versions.Google.dagger}",
      "com.google.dagger:dagger-android-support:${Versions.Google.dagger}"
  )*/
  // And here, all the libraries that we are going to use in our two modules which are module1 and module2
  val libraries = arrayOf(
      // Retrofit + OkHttp + Gson
      "com.squareup.okhttp3:okhttp:${Versions.Libraries.okHttp}",
      "com.squareup.okhttp3:logging-interceptor:${Versions.Libraries.okHttp}",
      "com.squareup.okhttp3:okhttp-urlconnection:${Versions.Libraries.okHttp}",
      "com.squareup.retrofit2:retrofit:${Versions.Libraries.retrofit}",
      "com.squareup.retrofit2:converter-gson:${Versions.Libraries.retrofit}",
      "com.squareup.retrofit2:adapter-rxjava2:${Versions.Libraries.retrofit}",
      "com.squareup.retrofit2:retrofit-mock:${Versions.Libraries.retrofit}",
      // Stetho
      "com.facebook.stetho:stetho-okhttp3:${Versions.Libraries.stetho}",
      // RxJava, RxAndroid and RxRelay
      "io.reactivex.rxjava2:rxandroid:${Versions.Libraries.rxAndroid}",
      "io.reactivex.rxjava2:rxjava:${Versions.Libraries.rxJava}",
      // Dagger2
      "com.google.dagger:dagger:${Versions.Libraries.dagger}",
      "com.google.dagger:dagger-android-support:${Versions.Libraries.dagger}",
      // ButterKnife
      "com.jakewharton:butterknife:${Versions.Libraries.butterknife}",

//      "com.jakewharton.rxrelay2:rxrelay:${Versions.Libraries.rxRelay}",
      // Room
//      "android.arch.persistence.room:runtime:${Versions.Libraries.room}",
//      "android.arch.persistence.room:rxjava2:${Versions.Libraries.room}",
      // Paging
//      "android.arch.paging:runtime:${Versions.Libraries.paging}",
      // TimberKotlin
//      "com.github.ajalt:timberkt:${Versions.Libraries.timber}",
      // LeakCanary
      "com.squareup.leakcanary:leakcanary-android:${Versions.Libraries.leakCanary}",
//      "com.squareup.leakcanary:leakcanary-android-no-op:${Versions.Libraries.leakCanary}",
      "com.squareup.leakcanary:leakcanary-support-fragment:${Versions.Libraries.leakCanary}"
      // Glide
//      "com.github.bumptech.glide:glide:${Versions.Libraries.glide}"
  )
  val annotations = arrayOf(
      // Room
//      "android.arch.persistence.room:compiler:${Versions.Libraries.room}",
      // ButterKnife
      "com.jakewharton:butterknife-compiler:${Versions.Libraries.butterknife}",
      // Dagger
      "com.google.dagger:dagger-compiler:${Versions.Libraries.dagger}",
      "com.google.dagger:dagger-android-processor:${Versions.Libraries.dagger}"
      // Glide
//      "com.github.bumptech.glide:compiler:${Versions.Libraries.glide}"
  )
}