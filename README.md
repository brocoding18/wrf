# WRF

Приложение для сотрудников.

Тестовый пользователь: +79227091489/1234567

Дока на бэк: http://summergirls.ru:8082/api/doc (dev/switch)

Гайд-лайн для оформления **README**: https://my.usgs.gov/bitbucket/projects/CDI/repos/cdi-2016-virtual-training/browse/making-a-nicely-formatted-readme-in-markdown.md and https://bitbucket.org/tutorials/markdowndemo/src/master/README.md

**master-dirty** - изначальный исходный код проекта

**master** - Clean + MVP + Dagger2 + RxJava2 + Retrofit2 + Cicerone

**master-coroutines** - Clean + MVP + Dagger2 + RxJava2 + Retrofit2 + Cicerone

**master-mvvm-architecture-components** - Clean + MVVM + ArchitectureComponents + LiveData

Обязательно сделать:

    1. Написать свой код-стайл для Kotlin
    2. Перевести все приложение на Clean Architecture
    3. Перевести DI на Subcomponents